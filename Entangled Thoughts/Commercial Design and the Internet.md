# Commercial Design, the Internet, and Progress

The state of commercial design has lost its way. There is no connection to the traditions which constructed the situation which could precipitate the industrial revolution, the starting gun, set the ball rolling, it has never slowed, only ever accelerated. As the speed of revolution increases the care and attention that can be given to the minutia points, and relations to previous|future interactions in space and time drops.

If we can't allow ourselves to slow down, abandon any notion that it is possible to capture any market in totality, or that increasing market capture is a good thing. With little to no regard of whether the production service being tendered is any good, still, in the situation it finds itself when it finally rears its head off the paper page of the board room table.

As the Internet destroys boundaries, borders, regulations, or the reach of any control system regardless of how hard it tries, the ability of anyone regardless their proficiency in any particular field, can, with attention and care become as useful as any other in the situation within which they find themselves. 

Why pay someone to fix something for you, when the internet can tell you how to do it, and, importantly what to do if you screw it up. There will always be someone who has done it before you, or someone wasting their time on some forum willing to try working it out with you if you can't find the person who did it before you. As information spreads, and the idea that predatory services are worthwhile dies; and as such, their predatory behaviour exponentially increases.

The state is no different, as it feels itself losing control, losing grip of the reins of the society it sits behind; it lashes out with control, regulation, and stifles criticism or opposition to itself.  
3/25/2021 8:43:59 PM 



# Postmodernism, culture and politics. Deleuze, and the diagonal.


> "The clash of reciprocally-negating arguments never moves forward. There is no 'debate'. Nothing ever comes of it other than a hardening of identities premised entirely on opposition to being negated by the other. Hollow at the core.
> It is, rather, about speaking for others, in 'debate' with others who speak for others. Those who are represented become a market share. Entire platforms are now designed to monetize this. 
> Culture moves when it takes the diagonal. Deleuze was right: any interesting writing is monologic." 

> - Who wrote things in forums online means little to me



The problem of politics in the present society is postmodern. A tenacious term, worthless of definition by its own definition.

And as such the solution to the problem of politics is the present society is post-modern. A state uncomfortable to many who've not come from a basis of understanding and advocation of a communistic world view. The idea that no one is important, everything replaceable; and production, consumption.

In my view technological development drives cultural production, which leads to turnings in the social and political strata's of society. And ultimately who cares about the political strata of society, if it is driven by forces external to its control. Tell me, what is the point in caring and involving oneself in extant politics?

In relation to those fully capable of an accurate or thorough explanation of the ideas or concepts I wish to introduce, this will be wholly inadequate. However, I do not think, precisely, a thorough, complete, or accurate explanation is entirely necessary in this case.

- Arborescent and Rhizomatic thinking
- Assemblages, Multiplicities, and Stratum
- Strata, Lines of Flight, and Spatium (3dimensional strata) - file:///U:/Users/user/Downloads/PAPER%203%20PROPOSED%203D%20CADASTRE%20LEGISLATION.pdf
- The Body Without Organs
- The Desiring Machine


- - -

A mode of interacting with thinking about the world. Everything is relevant, how is what is unclear. 
The framework beguiles the utility of interacting with everything and anything which presents itself; for example, Geological phraseology and syntax as an obscured fascimilie of all that builds by layers, is affected by time, and great cataclysm.

The quantity of output has become problematic, more is created than can be consumed, from within my view, the solution seems to become an exacerbation of the apparent underlying problem, that there being more created than can be consumed cannot become the reason by which justification of inaction, or attempted destruction is applied.

In short, do. Anything. Consistently. But define how you justify the continuation of your consistency to yourself for yourself, because, ultimately, that's all that matters.
 


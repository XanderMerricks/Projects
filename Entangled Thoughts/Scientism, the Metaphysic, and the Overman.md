# Scientism, the Metaphysic, and the Overman.


"...a lot of people say that their approach to life is scientific - as distinct from metaphysical - and that metaphysics is bosh anyway. But evrybody - by virtue of being a human being -is, willy-nilly, a metaphysician....." Alan Watts [Learn the Human Game](https://youtu.be/7moy4kCn2ig)

That is to say, what fundamental assumptions for what he wants? What are his axioms for living? Watts continues suggesting he has found '*Psychologists*', or '*Psychiatrists* "tend to be blind to these fundamental assumptions, but they tend to feel that they are scientists, because that,  of course, is fashionable in our age.

To be a scientist, or to have based and thereby derived a morality, or structure of systematised values, requires adherence to the orthodox, and academic way to discuss, or to do things. As the *Axiomatic* strata which gives rise to the scientific system precludes the adaption of such an axiomatic system based on the merits of their foundational nature.

This foundational nature, constructs the fundamental assumptions by which the living of a *'A Good Life'* can be achieved or eked out overtime demarkated by alterations, leaps forward, or rapid changes (paradigm shifts) within the axiomatically defined system of Science, with a capital S. This capital Science requires an adherence to a Scientistic view of the world, as all that can exist, exists along pre-defined lines of axiomatic arrangement: 

>"If there is one thing, and another thing; that will always mean there are two things."

This *Scientism* is at base a range or set of assumptions about how an individual entity can expect to have the world respond to their existence: 

>"If it is very cloudy, and I do not take a waterproof coat outside with me; I will get experience more water from the sky than I would've on a sunny day.

This combinatorial method of visualising experience in the world is at base also Scientistic, because it relies upon and debases itself by yielding to a cascading collapse within the internal system from exploratory discussion of specified meaning.

There is, it would seem, very little point in the idea of political or socio-economic push boxes. What utility is gained by a resortation to an axiomatically designated, systemically delineated, multiplicity of overlapping meaning; as a means of differentiating belief from individual, and individuals from their beliefs.

The very idea that there is an argument, or debate, or even discussion -to be had- over what these socio-economically-political descriptors which lead towards this neo-post-modern state whereby sets of, and individuals themselves are Scientistically ordered, and categorised into straterised groups. This in turn facilitates the abdication of any one individuals duty to themselves to believe in their own beliefs. To exercise their own sense of the moral duty. Or to demand alignment with an axiomatically defined foundation for anothers *morally right* way to exist for themselves.

Everyone is their own man in a world that does not want them, debasing oneself to the debauchery of the Scientistic world view facilitates a descent into which structures of fixed unyielding axiomatically defined arenas unable to be altered, or redrawn at will by will.

I am slowly in the process of writing a piece entitled "*The Strata are the Judgements of God*" inspired by a man named Mark Dyal's four part piece on *Deleuze, and the New Right*, which has been put to endless delays, as I cannot seem to put myself back in the right frame of mind to complete or continue any of it. 

However this idea is the suggestion towards the ideals which in turn overcomes the overman. The idea is that you can be better than all around you, but only by your will, no one else will will you into the being for which you are capable of inhabiting. You the individual inhabit the meta-temporal-space for the conscious world in which you live, my contention is that these are merely layers of analysis at which different changes to the world can be made. 

An example to conclude: There are often things you want to do on the PC, and some of them require using the Terminal, and some of those require you to elevate the Terminal to a higher power state to overcome the boundaries of the axiomatic system which had been constructed within the system of the Adminstrator protection.

You are your own administrator level protections. 

Use them.

And Use Them Wisely.
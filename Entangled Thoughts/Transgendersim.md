

# Transgenderism

## How to commit suicide.

I have traversed and survived a tunnel of dysphoria attempting to annihilate my internal self as an attempt to replace myself with some caricature of the people I see in the world who I'd rather inhabit than myself.

To consider the issue of whether it is or should be possible for an individual to destroy themselves to replace the self with a façade constructed from an idealised version of the world as presented externally.

To seek the total destruction of the self in the world is deeply attractive, and magnetic in its pull. There is a problem however when the deaths of individuals is treated as the treatment of social gender based confusion or dysphoria.

What does this transition or translation across the social sphere do to the people who make up that social sphere, how does someone consolidate into their reality the functional loss of an individual in their life, but without the totality over time, as their physical entity has been masked by another person.

The totality of the transition deletes the previous individual, not just in the physical space of reality, but in the thought space of self as constructed by language. Is to transition, to kill yourself solely in the minds of other people. Whilst continuing to live within themselves. 

It would appear to me deeply selfish to abdicate your responsibility to the other people you know to sustain yourself as yourself over time. To change persona and personality over time, specifically into a distinctly different person seems to prevent the transitioned to live as they would want. Whilst simultaneously seems to say that the individual transitioning does not think the world they live in would allow them to live as they would want; which, to me, would suggest that it is a problem of perceived perception rather than a contradictory cognitive inability to consolidate the duality of the self, by in effect "switching sides".
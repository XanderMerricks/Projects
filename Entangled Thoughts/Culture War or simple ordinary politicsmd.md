# Culture War, or simply ordinary politics?
## The Americanisation of a European Philosophy

We are sold a society deeply divided on a broad range of issues. Of a society deeply embroiled in a *'culture War'*. I'm not sure I agree.

To consolidate the point I want to get at is summarised below:

> Are the divisions in contemporary society a series of disputes within a single culture, or as a struggle between competing cultures?

Islamofascism, Ecofascism, and Ethnofascism

Protestantism and Catholicism


Bill Hopkins, Jonathan Bowden, and the Young Angry Men





Statues and their place in society

Where this becomes particularly interesting for our discussion of the nature of the culture
wars divide is in the response to these proposals by those opposed to removing these statues.
For, echoing the “adversary culture” argument that has been one of the orthodox camp’s
favourite talking points throughout the culture wars, rather than engaging in a political
debate surrounding the virtues or vices of the people portrayed in these statues or the
socio-cultural consequences of their being displayed prominently in city centres, those
opposed to removing these statues have often simply flat-out denied that they are political
at all. These statues, so the argument goes, are simply “history”, a neutral record
of the past; they are not inherently political, it is only the protestations of those questioning
their presence that has made them so. As a result, they argue, we simply don’t need
to be having this conversation. What we see, then, is less a political debate than a debate
about what we should consider “political” in the first place. - https://youtu.be/TJ8ws2dqqFg?t=2407






Politics as an arena or a process.    -    The space of Governance or the procedure 
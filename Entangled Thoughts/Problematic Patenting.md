# Problematic Patenting: The Potential for a Disruptive Shift in Design Ecosystems.

New designers are the products of their respective educational systems, there are many pathways available to take from childhood to designer. Whilst simultaneously the definition of �the designer� is broadening and diversifying such that traditional routes into the design world are losing their hegemony on design ecosystems. 
The relationship between designers and the ecosystem within which they design is complicated and dependant on many factors, many of which are not easily visible. 
It seems to me in some way that the most significant problem facing designers is the overprevelance of information, with no updated methods for appropriately sifting through the material. Virginia Woolf in A Room of One's Own describes, whilst visiting the British Museum to investigate Women, finds a more troubling problem.  

�so I pondered until all such frivolous thoughts were ended by an avalanche of books sliding down on to the desk in front of me. Now the trouble began. The student who has been trained in research at Oxbridge has no doubt some method of shepherding his question past all distractions till it runs into its answer as a sheep runs into its pen. The student by my side, for instance, who was copying assiduously from a scientific manual, was, I felt sure, extracting pure nuggets of the essential ore every ten minutes or so. His little grunts of satisfaction indicated so much. But if, unfortunately, one has had no training in a university, the question far from being shepherded to its one flies like a frightened flock hither and thither, Helter-skelter, pursued by a whole pack of hounds� - Virginia Woolf (1928 pp. 32-33)

This is no longer a simple problem of there being too many printed books from which to extract some meaning, the quantity of information at our fingertips is astronomical. Woolf suggests that a training in shepherding of a question is the value of the university, in particular Oxbridge - hence their glorified social standing as the best universities. 


There is a fundamental problem in how to quantify and qualitative an individual�s labour, cognitive or otherwise. This leads to a distortion in the exchange-value in relation to the functional use-value of a product. Which in turn diminishes the perceived importance of the functional use-value proposition by which metric the designer is expected to market their products. 



Are we all commodities? is everything a commodity, can it be; should it be?

There are many questions, is art a commodity, is the art piece the commodity, or is the collective memory or connection to the artist the commodity?
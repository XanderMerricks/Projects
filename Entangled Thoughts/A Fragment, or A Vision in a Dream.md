#A Fragment, or A Vision in a Dream

## This is the Door
I open my eye and step through the door into the garden. This garden is unlike anything I have seen in the world, it displays things that are impossible in the real world.
I look down across to an ordered garden laid out as if a mandala of foliage set against a backdrop of sweeping green hills topped by perfectly placed and shaped trees under which to sit and contemplate. In the centre of this Mandala sits a non-descript shed type building which I walk down towards.
As I walk down the gentle slope the paths seem to morph from a simple pattern of nesting squares into an impossible connection of passages and canals indicative of an Escher drawing, I find the pattern it creates completely logical even in its impossibility.
I traverse what seems an endless Labyrinth of passages and stairways, although after what seems no time at all I end up in the centre and I look back down a dead straight path about twenty meters long back to where I think I entered this garden, although I am no longer sure that that was the way out again.
I turn again back to the door behind me to find it has changed to what looks like a Victorian terrace doorway, the door however appears to morph and change as I look at it, this morphing seems to perfectly reflect my current mood, even though I couldn’t myself attribute the colours and shapes to feelings they seem to communicate to me.
I step through this doorway to find a semi-circular room with doors along the rounded edge each door has no distinct colour until I come close to them. There is nothing to say which door is which, although despite this I know which door I need to progress through to find what I seek. I pass through the crimson door and enter my library; I pass along a passageway lined to the ceiling with bookshelves. These books have no titles or labels, and there are large spaces left in which to add to the collection. At the end of this passageway the room opens up to reveal a circular space with shelves extending at every opportunity, and placed in the centre is what I am looking for.
I approach the lectern and see the book placed upon it, the book is large, leather bound, and well worn. All things I expect to find, just as I left it. Reassured that still no-one else can reach me here I open the book.

## The Garden

I concentrate.
Breathe.
Breathe again.
The colours, shapes, and lights spin and flutter.
Every colour and shape Imaginable spin somewhere in this perceptible field.
I see these waters pass, flowing away, there's nowhere for them to move too.
Out of these depths my familiar eye surfaces, the colours spin as a vortex through me.
My eye opens, the pupil an ever sinking pool of black, here I see where these waters flow.
I breathe.
Slowly blink.
The colours seem to shimmer, am I losing my control?
I breathe again.
Step forwards, my garden awaits.

This is my doorway, my minds eye opens wide and my carefully cultivated garden shimmers to be. I check everything is in order, first scanning down the gentle slopes into the mandala of coloured plants and flowers, thronged around by low hills with easy trails traversing them into sparsely wooded areas, I see trees of my own design and an ever morphing foliage of those outside my conscious control.
The smooth paving runs down towards the centre of my garden seemingly passing through the complex layers of plants that make up the mandala I see. 
Upon each step down towards my goal the images move and change, upon each step my garden opens to welcome me back.
The pathways and canals upon which I do not traverse contort into escheresque routes of impossible perspective. 
Soon I come across my door, this is the most precious part for behind my innermost thoughts are held. I feel an immense rush of power stepping across the threshold into a vaulted chamber of pentagonal shape, I stand in the middle in awe of my own creational might.


## Perspectives
I do not have the tools require to interact with the society within which I exist, there are too many other perspectives from which objective facts can be observed to make them simply subjective based on perspective. There is however, unfortunately, an infinite number of perspectives. Even if you think you have encircled and encased within a sphere all externalities for perspective of an object, but still you miss almost everything, the inside of the object is hidden, and the back, concealed by the front from being consolidated into an objective reality.

> Externalities | Internalities
	> Perspectives

A disaster of mind encroaches onwards, we fall away like nothing ever comes. How can it be that we are to be here? Why can it be that we never were able to be there? I cannot see why anyone would want to anything everytime. Time destroys and withers, dismantles and deconstructs. Space dissipates within a whimper. I am a simpering fool gazing over the brow of a hill whilst leaning against the gate behind my eyes. Nothing can be seen that is not simultaneously hiding that which can not be comprehended.

We are standing within the Garden of Eden, brimming with knowledge bursting to get out, constructing a Tower of Babel clawing for recognition from beyond the firmament, we claw and snarl at the world, demanding it recognise us and our existence, we pity our own failures, unendingly, even when we can no longer see whether we have reached where we set out, the path extends forever onwards, stripping us of our persons, pulling everything away digitising the pain, pulling, pulling, tearing, the pathway of strife. 

The Dark is nothing without the Light, and neither are something without an object to give them both form. The light exists not without a form to cast shadow within. We cannot consolidate ourselves whilst we place ourselves solely in the perspective whereby there is no shadow, to totalitise your hiding from the shadow eats at your being, to dive into shadow consumes you in totality, an agent of capture. To balance on the precipice of the ball cast shadow, the rim of the world. the edge of the light, the firmament of heavens. Balance, upon the rope *Zu Ubermensch*. Content, not happy, satisfied, not distraught, contentment; meaning, a life we see. Traverse the rope at peril, treating the ends a goal, rather than a means to a goal removes the flight *Zu Ubermensch* to find the point of balance, rather than the concrete points of stability.



## The Pen against the Sword
The silence in the brain is ever comforting however fleeting and rare. The thoughts course through me as a rushing torrent of constant nonsense when not directed by the pen.
They say the pen is lighter than the sword. I would say it depends on the scenario. The reflex and control needed to weild a sword requires the absence of this never ending torrent of consciousness one is required to experience.
When weilded in sport the sword, I feel can be the pens equal.
Where the pen dictates, the sword dances.
Where the pen controls and stabilises the mind; the sword shapes and dances with the mind .to weild either effectively one must end the thinking that these things are themselves individual entities, but instead that they are themselves merely an extension of ones own mind.
Only in this way is success found. And the still, calm serenity reinstates.






## Losing people to the sands of time, On Death

I was reading a Guardian article about a podcast series they were releasing by Mark Longley on surviving his daughters murder. The last paragraph quote from him drove my brains thinking into overdrive; connecting, disconnecting, reaching around, and examining many disparate thoughts.

> “People keep asking if you have closure. You never have closure. I think people wanted to feel I had closed that [the murder] off so we could all move on. You never move on. I miss Emily every day, I think about her every day. And, God forbid, if you ever did move on, it would be like saying that person didn’t exist.” - [Mark Longley](https://www.theguardian.com/tv-and-radio/2019/jun/11/what-a-load-of-rubbish-closure-is-mark-longley-on-surviving-his-daughters)
- - -
>Eleanor Ainge Roy - Monday 10 June 2019 19:00 BST (Accessed Wednesday 12 June 2019 13:00 GMT)

- - -

Many other articles published in the Guardian on the topic of Death or loss and Grief, for me simply miss the point, there is no consolidation | closure, there's only ever just more time since it happened.

My Fathers death sits with me with whatever I am doing, the presentness of these thoughts waxes and wanes in stability. It is however, no longer something that cuts into the centre of my being, and is something I can freely talk about with (so far) whoever I meet.

In January of this year, in the week before his passings decade anniversary, I had tattooed "or, a vision in a dream. A Fragment" in lettering extrapolated from a page of scribbled addresses from his time in Bristol.

The content, the process of his final years, the expansive and pernicious nature of addiction. They sit with me. He stands above them; "This is what destroyed me", this is the path not to walk down. A guide book, the process by which to not destroy yourself and everyone around you. 

The consolidation of loss is never done, there is no closure, until life's closure; we march on through these dying fields. At least the dead don't walk any more. 

- - -

There is a second part to this story though. One that is much harder to put down into words, a conversation seemingly threatening to pull the very fabric of our reality asunder. 

"Can a person become another?"

"Can everyone else allow them to attempt to move the world on, in *spite of the scale* of loss?"

None of the words utilised in journals, and formal parlance allow the breadth of emotion to be felt. We have located a mechanism by which to transcend death, and have thence dutifully legislated a pathway for those whom existence as themselves seems no longer tenable, nor does the only other previously available solution; dying. 

Transcendence is life, and death; the passing from the Transcendental to the Fundamental. 




## Disintegration

I spent an hour or so wandering in the streets of the city in which I live around noon today. The air was heavy with the threat of globular rain, the bright powerful sun lay, when I went out, behind billowing shrouds of dark clouds.

I was primarily going to alleviate myself of excess video game carts from my teenage 'collecting' years, and was thinking about the incapacitating feeling I have over the sense of loss of value, and my inability to sufficiently sustain my capital over time. I knew as I crossed onto the level that I was only going to be in for a disappointment, getting someone else to do the laborious leg work of finding a buyer for my things cost a higher price these days.

I looked up as I felt the dappled sun shine through the lined trees, warming my feelings about the world, despite the ever looming, hanging clouds. I felt taken aback though, the debauched scene which presented itself to my eyes pushed through all of my thoughts. I was overcome with a sense that our society, the cultural world in which I live was teetering on the brink. Despite the threat of storming rains, a collection of grimy men accompanied with a speaker system, cans, and loud disjointed conversation. A man on a BMX bike, grown with dreadlocks and rasta hat in tow pushed off towards me, clearly linking something to someone, the openness of this act the lack of care of who or what I may have been, a discussion ensued over whether it was how much for one, three, five; deals for some, profit for others I guess. A hanger on I presumed, although it became clear that he was the facilitator was trying to take a cut from someone somehow for whatever it was that he had done, the groups of men were mere meters apart, I cannot see what he could've done really bar be loud about the prevalence of the access in the face of this other group.

I passed the three of them deep in conversation, feeling confused about how present their dealings was in the presence of my eye, I looked up anyway to look over the level, and was presented with the sight of the end of a man's penis peeing on the side, I can only assume he thought he was behind the tree, or peeing on the tree; although this was not the case at all, behind him was a tangle of bits of wood, tarps, and broken mattresses. 

I could not look at him longer than a glance, the way in which he was staring at the people behind me, more interested in how the dealings were going than for his dignity, the overflowing bins, the deteriorating grass verges covered by broken materials, the lack of care for the environment surrounding them astonished me, how does someone manage to ignore the deteriorating landscape in which they are existing. I have walked past this patch many many times over the past few years, yet now they seem to be encamping and subsequently destroying. They can only surely spread. 






























I read the first line on the page to myself: “Do not worry yourself. There is nothing that can harm you
here that is what happens on the outside”. I instantly feel myself slowdown and the anxiety in my
body seep away. When my body feels completely calm and at rest I take a deep breath…

I open my eyes to the crowd in front of me, the stillness however persists, I feel ready.

“The State of the nation in which we find ourselves in tatters, this is as a result of what the current
government insists is required for the benefit of all.”
I pause, I have done this before and know what to say, I have practiced these lines deep in my
library, they might as well be burned into the back of my eyelids.
“What we must do as individuals is demonstrate to these people that we know that what they say is
merely based upon lies and rhetoric.”
“The idea that a government need not answer its citizens is based in despotism; we must all in this
view unite against what oppresses our view of the perfect world.”
“This may not be possible in our individual life times, but we must not shy from the responsibility to
our future generations. If we lie complacent in the fear of this government nothing will change and
the situation can only get worse.
The idea that things will just get passively better for all of us is just a fallacy and a delusion played on
our minds by the propaganda imposed on our minds by this endless pervasive idea of fear. We must
not give into this fear, it will crush us!
Faced with an apparent impossible situation we must turn. Face forward. And walk towards what we
cannot see with our eyes. We must believe that it will be there when we arrive. Believe and you shall
make it so.
We shall face endless opposition to our cause, our bodies may die but they can never kill an idea.
Enter on this path at your own peril.
Martyrdom is possible along this route although if that is your goal in this, I would urge you to
reconsider your commitance to this cause.
You must never reveal to someone whom you cannot completely trust the nature of our movement;
this contains the jeopardy of us all, and in these early stages can lead to the death of our idea.
I shall leave the stage now to allow others to speak who are more versed in organisation than I. I end
with the message that if we can avoid detection at this moment we will rise, spread our message to
those in complete confidence and as we spread we will gain more momentum and soon our idea will
become unstoppable.
Individuals in the face of unquestionable power!”

Deflated I step aside and walk to the edge of the small platform on which I stand, the ground upon
which I stand appears to have shrunk and the crowd I saw before ripples into a small congregation of
familiar faces. I feel gladdened to notice faces amongst the group whom I do not recognise.

At the bottom of the stage I stop and ask [blank] whether all the new people have been checked to
prevent compromise.
[X] assures me that all of the proper precautions have been followed and that we have still yet to be
compromised.

I turn back again to the congregation elated by the fact that what mere months ago was just me and
a few friends in [blank] basement, has turned into a group large enough to hold small
demonstrations against this propaganda machine.
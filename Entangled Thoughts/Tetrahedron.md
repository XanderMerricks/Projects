
# Threes become One | Ones become Three


## A Tetrahedron | or A placing point for Zero


The finalality of our last conversation left me wanting of material to look at of which I might have something to understand.

The Platonic objects surround me everywhere, a harmony of mathematical perfection rolling around multiples of tetrahedrons infinitely tessellating, expanding, and contracting; swimming in colours.

The Tetrahedron is something of another place, a shadow of a shadow, bounded by an exterior. Many points in many places, projecting from somewhere to somewhere else. Rotating around time within throughout without itself. 

Squares, Triangles, even Pentagons project, even leap, from the edges by the light. Three from One, always in everything.

The exterior and interior of a four dimensional cube | or second order Tetrahedra is created upon the multiplication of a lower order form. Squares from Circles simply a test of the granularity of analysis.

Deconstructionalism leads its thinkers through difficult ravines of empty space, where things have been so thoroughly torn into their constituent elements to simply have become simple representations of the firmament within their existence. 

There seems to be something else there though, the approach to and contact with the zero|finality|singularity, leads the mind to disregard the contents, zero, nothing; simply is that.

To consolidate this thought that there is nothing, not even "no-"thing, just nothing, stands outside of the physical boundaries as analysable from an observer, there needs to be something to observe, something to observe from, and the thing doing the observing. The singular "no-"thing becomes three.

An endless cacophony of deconstructable entities collapsing effortlessly into platonic forms, cascading themselves into simpler and simpler forms, finally reaching zero - Tetrahedra, or the boundary between things.



## A Cube | or A Bridge from Whence Two becomes Three


Something at some point somewhere came from nothing nowhere. Things are abstractions of bridges between connections to other things.
# Neural-Networkisation


I wrote an article earlier this week for VILE, ostensibly for the UNWAShED Daily Show, or just as a collection of ideas for anyone willing to consider it. I have tried in the past to temper my language, and overall tone of my work, that was not like this.

The last week I watched fewer streams, spent less time inside, and used more of my time inside reading rather than listening to the ideas of others. Specifically in this case Jonathan Bowden, a man, I believe, to be seriously underutilised as a thinker and general orator, and producer; to the eternal detriment of people of all socio-political persuasions. If at least his overall manner is directed more specifically to those of the more traditional right wing.

Although, I think, what even does right mean, or more specifically what utility is there in the present time for traditional mechanisms and methods for describing socio-political persuasions. We no longer live in a simply layered society, tectonic shifts have occurred overtime, leading to an increasingly disjointed and striated society in which the lines and layers between people are no longer in simple alignments or at regular intersections. 

This has led to a *'post-modern'* society, in which the interrelation between people, groups, counties,and territories, does not construct a simple and ordered hierarchy from which the society emerges. Facilitated by the nomadic nature of the Internet and interrelated fractal communities, society has been 'liberated' from the simple and ordered hierarchy which gave rise to it. 

I think ultimately the subconscious societal *'struggle'* is over what this 'liberation' means, or which 'side' or 'sides' gets to decide, or whether deciding is an option. That is, in my view, the functional advantage of the development of the Internet gives modern societies over pre-modern societies in terms of internal-societal collapse of territories or countries, is that the Internet allows a nomadisation of societal fragments within physical countries, of pre-collapse societal categories. 

The deconstruction of modernity into a post-modern society, combined with the power and interconnected nature of the internet will, in my view, tend towards decentralisation of the control of the function of the networks into internally fractal layered hierarchies of individuals in a representative structure of extant hierarchies in natural systems. 

To conclude this piece - thought more accurately perhaps, as I tend to write verbatim from my mind and rarely edit much later - I am reminded of a thing I had heard, of research into the fundamental physical reality all the way up and down is neural networks. As in the “possibility that a microscopic neural network is the fundamental structure and everything else, i.e. quantum mechanics, general relativity and macroscopic observers, emerges from it,”. [Vanchurin 2020](https://arxiv.org/pdf/2008.01540.pdf)

∴

The internet is facilitating the neural-networkisation of global societies into tightly interconnected, nichely ordered clusters.
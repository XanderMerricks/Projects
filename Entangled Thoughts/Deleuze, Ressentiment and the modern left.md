# Deleuze, Ressentiment and the modern left


"I'm still on Deleuze on Nietzche and I've gotta say its a shame that the only piece I can find about how drenched the modern left is in ressentiment is by some right wing chud because there is something very interesting to be explored there." - [@isaac_kh](https://twitter.com/gregkfoley/status/1380854777261793283)

---

![https://twitter.com/gregkfoley/status/1380854777261793283/photo/1](EynIGXaWQAAHCY7.jpeg)

---

> No way. The idea of feeling guilty is, for me, just as repugnant as being some else's guilty conscience.


There is something to be learnt here, as elsewhere on the internet. The power, or lack thereof, of un-understanding, or un-knowing of political or philosophical elements, thoughts, or ideas. When placed in 'combat' or 'conflict' with another of an apparently larger, greater, or more sophisticated understanding, or knowing.

The internet is a machine for which exists for the means of efficient transfer, storage, and communication of information. The *ability to know* is an extremely powerful weapon which is wielded in many different directions. As in, the state of un-knowing can reasonably seen to be unreasonable given the seeming prevalence of the existence of the access to information of any given thing from any given direction; and as such stands bared against criticism. Likewise the state of (too much) knowing can reasonably seen to be unreasonable in the same manner as before. 

A stable position reinforced by bulwarks of relevant information against a rising tide. Despite this however, any reasonably considered stable position is a foundation-less dam un-braced against a changing scene; or when simply viewed from any turn of any degree towards a tangential position. This inherent instability in the ability to hold and reinforce stable positions necessarily leaves behind and runs rough-shod over any and all unwilling or unable to manipulate their views or positions to reorient them in the face of changing tides, or rolling seas.

Nature moves in procession, in an attempt to forestall the turning processional process would be akin to trying to prevent the machine below alone with your bare hands whilst it's running at full power.

![bucket wheel excavator](bucket+wheel+excavator.jpeg)

> [Bowden quote - I'm stepping over that]
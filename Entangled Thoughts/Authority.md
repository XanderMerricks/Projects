# Authority, or, What do you want to do?

I have been asked my entire life throughout all systems of authority, what it is that I would like to spend my future time doing; presumably for money.

"What do you want to do?" 

One of the hardest questions to answer, or even to ask of yourself. I make things, supposedly, something that used to be called design. What is the answer to the question when what I want to do no longer appears to be a worthwhile thing to be doing? Product creation, profit margins, and GDP are outrageous measures of things. We demand of our people a plan, at least implicitly; one they tell other people. The economy as measured by GDP is a slightly strange metric via which to attempt to extrapolate the size, or success of a system.


There are already too many people on the facets of the deep, toiling at the face of the society, destroying themselves for the further expansion of a digital image, one can extend and expand themselves over the face without even really moving, the exclusivity of the social holes exposes our fascination with what other people know, that we do not already know. Currency becomes a transitional medium by which information technology can be traded. A simple go between, something of nothing, or no-value; only transition.

It would be better surely to dismantle this idea of pecuniary currency as having inherent value as a physical object, the physicality of the monetary object needs to be irrelevant in the face of it's objective usability as a infinitely trade-able item.

Data is emerging as this something of nothing, or is being treated as such at least. People freely give up their data for nothing, and entities somehow have commercialised and monetised data. 






## Fascism

Ideological terms should not be used as verbs.

F	A	S	C	I	S	M
Fasc-ism

Fascination - To Bewitch

To evoke an intense interest or attraction
to make motionless : to spellbind
to be irresistible charming or attractive to

- - -

To Stupify

Fascino - I enchant | Bewitch



-ism, suffix which creates ideologies.


- - - 


Is Fascism the ideological marker for one which evokes an intense interest or attraction via means which attempt to make motionless people by being irresistibly charming or attractive to their fundamental ideals. 
#The Irish Question

## The British Isles, combat, confusion, division, and union.

There is an Irish question on our beautiful and wonderful Islands. We British of the British Isles have problems, we live in division, driven forwards by the history of the English. The Irish question is of unity, are we collected in our unity and love for our Islands, and by extension our separated desire for Nationhood. 

The global climate of international law and capital has been driving our peoples apart from each other for centuries. We must not at this critical juncture avoid the pursuit of the de-fragmentation of our Island Nations, collected in union as the British. English, Welsh, Scottish, Irish, and the other extended family of Ethnic Nationalities which could make up a Neo-British identity as distinct from the 'melting-pot' envisioned by global capitalistic tendencies.

Collected we are greater than our separate identities are apart. Myself, we, the English must see what we have done to our islands, what our Monarchy has done to our peoples, what our government has done to our cultures. Her Majesty's Government has run its course, and no longer serves the interest of Her Majesty's People. We, People, are being destroyed; like we destroyed the world over. First in the 16th C. wherein we initiated sectarian animosity leading to catastrophe.

Our peoples together created the modern world, from precision to government, to collective union of peoples, and the idea of Nation. In the Burkean (September 17th 2020, Eoin Corcoran) writes on *Nationalism as a Moral Order* and outlines a framework for the Good as distinct from the Evil. That that which is evil is that which harms the Nation or its peoples, and that that which is good is not. And herein we find the Irish question:

"We extrapolate Evil hence:

Abortion is the murder of members of the Nation, and thus it is evil.

Mass migration is the dilution and destruction of the Nation, and thus it is evil.

International Capitalism is the impoverishment of the Nation, and thus it is evil.

International Law is the restriction of the Nation, and thus it is evil.

The Drug Trade destroys communities within the Nation, and thus it is evil.

And the same for Good:

The family unit strengthens the Nation, and thus it is good.

The Irish language unites us with our ancestors, and thus it is good.

The **unification of Ireland** mends a breakage in the Nation, and thus it is good.

Protecting the environment allows our Nation to survive, and thus it is good.

The punishing of criminals rectifies harms inflicted on the Nation, and thus it is good.

The crushing of the Red Menace saves the Nation, and thus it is good."

But does no good in mending the more severe breakage within the British Isles, that of the Irish against the English, as represented by Her Majesty's Government of Britain. The Irish are British, as the Scots, Welsh, English, and associated National peoples. We, and our countries are being radically altered behind our eyes, a UN schema of replacement utilising foreign wars based on further sectarian conflict is tearing our Nations and countrymen asunder.

We must collect as the National British against an International cabal of Capital and Laws fixated at a single juncture - in a sense of linguistical time - our heritage, connection to our history, representative symbols and figures are attacked; and we as a collected peoples drift further apart. Attacking a peoples for the crimes of their government is an asinine manner in which to respond to a disaporic problem.

We peoples of the British Isles are better than our recent history would suggest, our governemnts have taken pains to lead our People, Nation, and Cultural Heritage down the spiralling edge towards *Violence* and *Treachery*. Our government has taken itself - with us on a tether - down the *Nine Circles*, and has separated our ability to relate to the history of our civilisation, via our connections to each other.

Our Morality, and sense of duty has been exploited. The nature of our islands leaves our flanks exposed to European menaces, and more recently to Americans. With our focus on our individual Nationhood turned inwards against each of the constituent elements of these Isles, we have lost sight of our flanks, our history, and our purpose. It is our Morality that guides the modern world, and it is our Morality the modern world of International Finance Capital exploits to sow division between our Nations; separating and off-putting the power and potential Moral strength, fortitude our Islands could display, if not engaged or subjected to a sanguinary state of affairs. We should be sanguine for our future, the Irish question is not a question of the Irish, but of Her Majesty's Government, and their relationship to International Capital.

The Queen must fall, the Monarchy must remain. 


We people of the British Isles must unite, or be destroyed forever.
# What in the fuck is an NFT?
## in short "A non-fungible token (NFT) is a unit of data stored on a digital ledger, called a blockchain"



And so, what does any of that mean?


A digital ledger is like any other ledger, except in this case it is one maintained or is a blockchain. A blockchain is a technology initially proposed, for use as "a peer-to-peer electronic cash system" in the form of the [Bitcoin whitepaper](https://bitcoin.org/bitcoin.pdf).

Blockchain technology has developed significantly since the Nakamoto paper and the launch and subsequent development of the rise of the valuation of a *bitcoin*. One such developed blockchain is the Ethereum chain, Ethereum is a decentralised, open-source blockchain with smart contract functionality. [Some more on blockchains](https://blog.ethereum.org/2015/08/07/on-public-and-private-blockchains/)

A smart contract? -> ["a set of promises, specified in digital form, including protocols within which the parties perform on these promises"](https://en.wikipedia.org/wiki/Smart_contract#Etymology)

Anyway, we're here about NFTs, and I'll try not to drift too far off into the weeds.

Ultimately an NFT is a bundle of metadata encoded within the blockchain recording things like who created it, when, how many, among a host of other details you'd expect to find in metadata. This metadata also includes a record of addresses of owners, and transactions, combined with smart contracts to act as middlemen in the trading process. A publicly held record, or the ledger allows for digital provenance, ownership, distribution, and control of artworks or other data, in a (for a public blockchain well secured against 51% attacks) unchangeable and 'permanent' format.

I am a photographer, and within photography I think the perfect analogy for the utility and actual functionality for NFTs exists. If you think about film photography, the photographer will usually keep a catalogue of their film, and thereby photographs. They can at any point produce new prints, enlargements, among other things to replicate the image in the same or different forms. NFTs for me are just another form.

I think my age assists me in understanding and not feeling daunted or out of my depth by the idea of exclusive digital ownership or value. For my example I am going to refer to the video game Dota 2, because that is the one that I played, but the idea is so expansive today than before I stopped playing just a few years ago. 
![basher](basher.png) ![scythe](scythe.png)

My most valuable item on Steams internal item trading market (a capital capture scheme in reality) I obtained simply by playing the game, and rolling high on a random item drop

![RNGesus](RNGesus.jpg)

somewhere within the steam machine there will be a some kind of store of metadata of the kind which can be encoded by a blockchain.

The price value is driven by a complex interaction of popularity/desire, and demand/supply, given the apparent market price of this scythe it's popular, and scarce. Probably due to the overpowered nature of the hero in the late game, the fact the item is a representation of an in-game item the diffusal blade, and its age.

This is how the NFT market functions, the value or price of an object is dependant on a kind of consensus made trust and belief affecting the popularity/desire or lackthereof combined with the supply and demand of each thing.

I have a very cursorary understanding of how the thing works on a fundamentally technical level, in terms of just how the things are created, or how to write or understand a smart-contract. But to what extent is fundamental or intimate knowledge about the production process of other representations of art is required to create, trade, and display other forms of artworks or things. And so will leave others to do that, there are plenty of blogs, twitter threads, or articles about it which are not hard to find, but are not easy to understand I find.

The existence of society is built through consensus making mechanisms, and mediums of exchange. Blockchains, NFTs, some and 'crypto' related things are, in my view, a paradigm shifting system which is a consensus making mechanism, via it's medium of exchange. Simply, as in the case with bitcoin, your wallet is the bank, and the money, and the ledger, and the medium of exchange all at the same time. The consensus is made via the process of interacting and transacting with and through the system.

If Steam breaks all of the value in my 'wallet' or account will disappear, this system of value and consensus is centralised under a single corporation, this is antithetical to the idea of a public blockchain, and this possibility of a single point system failure whereby everything is wiped out is guarded against by decentralising the management, development, and theoretical optimisation of the network system.Theoretically anyone can become a part of the network to act as, now, various forms of facilitator or 'energy provider' to the existence and perpetuation of the network.

I fear I drifted too far into the weeds, but I hope this begins to explain some things.

An NFT is as a provenance certificate, what that means is dependant on presentation, and the level of consensus trust or reliability in its creator.

It may point only to a file hosted somewhere, it may 'represent' a physical object, an NFT is a bundle of metadata which does what whoever created it, owns it, transacted with or on it, collectively say it does. Much like any other abstract system which fundamentally only functions and survives because the people who believe in and rely upon it say it does. NFTs, blockchains, and whatever else is coming or is here already I am missing are only really another imagining of how to build a system for coming to consensuses on trust, truth, value, or whatever other application it ends up being applied to.




* [NFTs Weren't supposed to end like this - Anil Dash {The Atlantic}](https://www.theatlantic.com/ideas/archive/2021/04/nfts-werent-supposed-end-like/618488/)
* [Ethereum Whitepaper](https://ethereum.org/en/whitepaper/)
* [On Public and Private Blockchains - Vitalik Buterin](https://blog.ethereum.org/2015/08/07/on-public-and-private-blockchains/)
* [Ethereum](https://ethereum.org/en/)
* [Bitcoin Whitepaper](https://bitcoin.org/bitcoin.pdf)
* [Tezos Whitepaper](https://tezos.com/whitepaper.pdf)
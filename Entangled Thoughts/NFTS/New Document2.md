# What in the fuck is an NFT?

> Wikipedia : "A non-fungible token (NFT) is a unit of data stored on a digital ledger, called a blockchain"

But what does any of that mean?

A digital ledger is like any other except, for our purposes it is one maintained by, or exists as a blockchain. A blockchain is a technology an example of which is bitcoin, proposed in this [whitepaper](https://bitcoin.org/bitcoin.pdf), "a peer-to-peer electronic cash system".

A blockchain is a forest of merkle trees.

"Visualized, this structure resembles a tree. In the diagram below, "T" designates a transaction, "H" a hash. Note that the image is highly simplified; an average block contains over 500 transactions, not eight." - [Merkle Tree, Investopedia](https://www.investopedia.com/terms/m/merkle-tree.asp)

![Merlke Tree](MerkleTree.webp)

A NFT is a bundle of metadata encoded on a blockchain, recording information such as who created it and when, and a host of other details you'd expect to find in metadata. This metadata also contains a kind of network map of links. These links point to things like hosting sources, its own location on-chain, the addresses transacting, contracts called. The Non-fungible token is a merkle tree inside a block somewhere along the chain, what's inside it depends on what who created it put in it.

The blockchain's physical location, where the ledger is held as [Ethereum.org say of the EVM (Ethereum Virtual Machine)](https://ethereum.org/en/developers/docs/evm/) "The physical instantiation can't be described in the same way that one might point to a cloud or an oacean wave, but it does *exist* as one single entity maintained by thousands of connected computers running an Ethereum client."

Being a photographer creating NFTs, I think the best analogy for the utility and functionality of NFTs is through a comparison to film photography. If you think about a photographer working with film, you would expect them to keep a catalogue of their film. In keeping a box of negatives in the back of the studio, the photographer can at any point produce new prints, enlargements, among other preparations for use in other forms, or as placeholders. NFTs for me are just another form, and widen the arena for participation in the financialisation, commercialisation, archivisation, and representation of artworks in many forms.

The concept of exclusive digital ownership and the value thereof, is essentially the same as any other form of ownership in any other conventional situation. In a digital sense, we can consider the internal item trading market on Steam. Steam is a video game platform, marketplace, forum, store, and general middleman for the distribution and monetisation of video games on PC. 

I used to play a lot of a game called Dota 2, there is a marketplace and economy surrounding player character cosmetic items, I ended up with quite a large collection of items. Either through trading, buying, opening loot boxes (gambling), or post-game drops. The most valuable item I have in my inventory I obtained simply by playing the game, and rolling high on a random item drop.

![scythe](scythe.png) 
![RNGesus](RNGesus.jpg)

The price/value is driven by a complex interaction of popularity|desire and demand|supply. The scythe doesn't drop post game any more, and the dota2.fandom wiki page for it doesn't match the item I have, and so for some reason it's trading at £24 something.

Steam is a centralised system and the marketplace is internal, even if I were to sell the scythe the money would only be *steam-bucks* for use on steam alone. The fundamental idea behind the object however is analogous to use cases for some NFTs.

The difference with NFTs seems to me to be it's openness, decentralised foundations, and wild west levels of free trade. Appealing to some, not to others. I think the difference is what you make with or do with what you have.

I have a very rudimentary understanding of how the technology underpinning my [NFT 'mints'](https://www.hicetnunc.xyz/tz/tz1T9mwfpXQJqb5LJ2CZ6sd69K9vyo6xEPLW), or that of my collection. A lot of the criticism I have seen is justified. Having said that I have learnt a lot more than I thought I would have in the past month, although I feel, as in other disciplines/areas/subjects, the extent to which detailed expert knowledge of the subject is required to enjoy participating in it is an open forum for debate. I for one am [open](twitter.com/xanderhizome) to reasonable questions, conversation, or pointers where I have them.

It may point only to a file hosted somewhere, it may 'represent' a physical object, an NFT is a bundle of metadata which does what whoever created it, owns it, transacted with or on it, collectively say it does. Much like any other abstract system which only functions and survives because the people who believe in and rely upon it say it does. NFTs, blockchains, and whatever else is coming or is here already I am missing are only really another imagining of how to build a system for coming to consensus on trust, truth, value, or whatever other application it ends up being applied to.

* [NFTs Weren't supposed to end like this - Anil Dash {The Atlantic}](https://www.theatlantic.com/ideas/archive/2021/04/nfts-werent-supposed-end-like/618488/)
* [Ethereum Whitepaper](https://ethereum.org/en/whitepaper/)
* [On Public and Private Blockchains - Vitalik Buterin](https://blog.ethereum.org/2015/08/07/on-public-and-private-blockchains/)
* [Ethereum](https://ethereum.org/en/)
* [Bitcoin Whitepaper](https://bitcoin.org/bitcoin.pdf)
* [Tezos Whitepaper](https://tezos.com/whitepaper.pdf)
The Hammer and the Sword: A Treatise on the Third Position


Spectres hang in the cultural psyche, the 20th century consumes rational minds, rendering them incapable to see through the smoke of perpetual war.



The Hammer



The Sword



The Autochthonous and Autochthonic; Autochthonism|Autochthonist



"Through a history of public health, I look at how early 19th-century public health interventions were a product of a modern line of thought that has a dark side, leading to discrimination, authoritarianism, eugenics, and the Nazis. 

What does it mean for a body – flesh and bones – to be politicized? For the rhythm of heartbeats, the density of muscles, and the flow of the arteries to be molded and shaped by power?

What's the best way to rank citizens on a scale? To make the child’s body still, obedient, but strong? 

How far can we go in engineering modern utopian bodies? 
Is it possible to forge the iron of the national body through recommendations or if not, by force?

Throughout the 19th century, bodies emigrated in droves from the country to the city. Their stomachs were hungry, for food, for work. They crowded flesh on flesh into slums. “Little Ireland” in Manchester had two toilets between 250. 5 or more often slept in one bed. Cesspools and dunghills were everywhere.

At the same time, factory owners needed these bodies to be productive, energetic, malleable." - Then and Now [youtube]








Thinking is the important thing. Know what you are.

"What does Neitzsche believe? He believes that strength is moral glory, but courage is the highest form of morality. 

That life is hierarchical. 
That everything is elitist.
There's hierarchy in each individual. There's a hierarchy in every group of individuals. There's a hierarchy between groups of individuals.

Inequality is what right-wing ideas really mean. Right wing ideas aren't just a bit of flag waving and baiting a few Muslims. Right-wing ideas are spiritually about inequality, the left loves equality, he believes we're all the same, or we must be treated the same, and they believe that as a morality, as a moral good which will be imposed. We've been ruled by liberal ideas for many centuries, but in the most acute form in the last 50 years. Liberal ideas say that men and women are the same and are interchangeable, that war is morally bad, that all races are the same and should all live together. So the population that just exists in the country is just a zone, it's just an economic area, but every thing's based on nationalism and materialism and it's purely a calculation of economic self-interest. 

People are unequal. 75% of it is genetic and biological. Partly criminality is biological, predispositions of drug addictions are biological, intelligence is biological, beauty is biological, ferocity or a predisposition to it is biological, intellect is biological. 

You can do a bit. But you're born to be what you are. And we should celebrate what we were born to be, because we have created 90% of value in modernity. 

I am a botanist in many ways because I believe we created a modern world that has been taken away from what it could have been. The modern man and that which preceded it are not necessarily in complete opposition, if people with our thoughts have values ruled modernity, everything about this society would be at one level the same in every other respect completely different. People still drive, you know contemporary cars there'd still be jets, and still be super computers, and so on. But the texture and the nature of life will be different in every respect. 

How so? 
  Firstly cultures would be monoethnic 
  Secondly there would be a respect for the past glories of our civilization
  Thirdly we would not preface every attempt to be strong by saying "I'm sorry, I'm sorry for what we have done" 
We're not sorry!

There was a chap who came up to me and said: "wouldn't it be better if we presented ourselves as the victims?" And the problem with that, is that it's what everyone else does. 

And it can be done, because there are many white victims in this society now in the way that it's going. But if you concentrate on pain and defeat you will breed resentment and I believe that resentment and pity are the things to be avoided. Stoicism should be our way. Courage should be our way. When somebody pushes you, you press them back. When somebody's false to you, your false to them. When somebody's friendly to you, you are for them. You fight for your own country, and your own group, and your own culture, and your own civilization at your own level and in your own way, and when somebody says apologize for this or that you say "No! I. Regret. nothing." 

The glory of our time is not behind, us we can be great again. Greatness is in the mind and in the fist, and it's the strength which exists up here, which is the thing that we have to cultivate. I believe that strength comes from belief in things which are philosophically grounded and appear real to you. We must return to sets of ideas which suit us. That are cardinal for us. That are metaphysically objective and subjective. To see the flux and warp and weft of life, and its complicatedness, but know there are absolute standards upon which things are based. That for me is life: Fire, Energy, Glory, and Thinking.

Thinking is the important thing, being white isn't enough, being English isn't enough, being British isn't enough. know what you are! 

In this book to read about your own culture as a revolutionary act. Knowledge is power. Listen to high music. Go to the national gallery, it's free you can save hours in there, look at what we've produced as a group. This is what the Muslims teach their people: to be totally proud of what you are in your own confirmation of identity, because that entities divide. So if somebody says to you you are descended from brigands, which is no sense individually what that sort of contrary ideology is. You say "I'm not going to bother about figures, and who did what to whom. I have over come that!" - "Oh, I don't like the sound of that, that's a bit illiberal." And I'd say: you just say "liberalism is moral syphilis...[] and I'm stepping over it" - "I don't like the sound of that you sound like a bit of a fascist to me." And I'd say "there's nothing wrong with fascism, nothing wrong with fascism at all."

If we can't overcome the weapons which are used against us we will disappear. When the Greeks sacked a city in internal warfare, everyone would be slaved. But they did not remember when their bard sang of their victories that they had denied the human rights of other Greek city-states, no people can survive if it incorporates as a mental substructure an anti-heroic myth about itself!" - Jonathan Bowden: Liberalism is Moral Syphilis







Oswald Mosley - on manifesto

"That is the real issue raised by a manifesto. You will not agree with us if you are content with things as they are. If you are willing to let this country just drift to design [desire?], but you will at least get us with sympathy if you are aware of the gravity of the national situation. If you believe that government by talk must at last give place to government by action! 

To believe that we cannot muddle through this time, but we also believe that with effort, and with organization, this country can be greater and more prosperous than ever before. We do not propose dictatorship, because the control of an elected parliament is still retained, but we do propose a drastic revision of the parliamentary machine, in order that the will of the people may be carried out. We have no real democracy at the present time, because again and again since the war, the country has voted for great changes and for decisive action. Yet again and again, their will has been thwarted by obstruction in the talking shop at Westminster. True democracy only begins when the will of the people is carried out. For that purpose we propose a new machine. A fight lies before us. A fight for action, for vigour, for vitality and manhood in government, a fight against the forces of drift of despair"
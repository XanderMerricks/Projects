# Ephimera Application


## Artist Bio
I am interested in the photographic artwork, images produced by photo-compositing, and frame animations. Following the culmination of my course and nature of the global crisis unfolding at the time, I turned to spend more of my time through the lens of my camera. Throughout this time I discovered the Crypto-art space, NFTs, Tokenisation more broadly. In combination with my background in product design, and love of photography, I feel I have discovered an artistic calling, and space to experiment with the painting with light through lens and sensor. The capacity to bridge the existence of physical and digital representations of photographically produced artwork is paradigm shifting.

## Medium
Light 'painting' | I use a canon 400D camera as a primary means of capturing photographic material, I recently acquired a 360 degree camera, and occasionally use an Olympus OM-2 35mm film camera. 

## Portfolio Link


## Social Media Links
Twitter

Instagram

Flickr

## Artist CV Link
Name and contact details
Alexander S. Merricks; Born: Cambridge, 00:07, 07, July, 1997. 

I currently live in Brighton, England where I went to university to study product design. 


### Experience
I've not been publicly producing artworks for long enough yet in my opinion to have had the time or exposure to any very relevant experience in this case. I have been photographing things for as long as I can remember, I come from a family of photographers, my Great-grandfather was a photographer who gave my mother her first camera when she was a child; which is the film camera I use when I shoot on film. There has, for me, always been something significant in the ability of a photograph to contain ideas, memories, emotions, and feelings. 

I've mostly only shot photographs in a documentary capacity, for myself, and others around me in the future to look back on, as literal windows into the past. 

Since I graduated, and have had almost a year to sit and think, and use my camera in a different way. I think there is something different about lens-based-painting as I like to put it from traditional pigment picture production. My DSLR captures light as it is through lenses of glass onto a digital sensor; I primarily, or almost exclusively at this point in time, see my own images on screens which are also light. It is almost as if there are two hands of colour one of light, and one of pigment- add all of one to get white, add all of the other to get black. Within this idea the digitally represented photograph is even more so literally a window into another moment, time, or place in that the photograph is the literal captured representation of the incoming light to the sensor, and the projected image on a screen is like a time-shifted literalistic expression of the same moment, time, or place using the screens light and the cameras sensor as a "Time And Relative Dimension In Space" machine.  


### Education
Product Design BA (Hons) 2:2 - University of Brighton, Brighton, England 2016-2020
	- Theory and Philisophical foundations for a methodology of design; holistic view of the productive and creative process; exposure to a wide range of methodologies, and processes; broad view of the structure of the existence of objects.




### Tools
Camera:		Canon EOS 400D, Olympus OM-2  
Software: 	Adobe: Lightroom, Photoshop, Indesign, Illustrator; various photographic image related tools, 'AI' algorithmic generation and manipulation tools.



## Upcoming Artwork

1. Photocomposite - Hanging Man
2. Colours Series 1 - Camera edition of 7 R-O-Y-G-B-I-V
3. Animation (Sky Drawing backwards|forwards maybe)


## Upcoming Artwork Descriptions

1. Animations, moving graphics, photographically produced frame animations
2. Photographic composites, layered imagery, focus of figure, evolving series involving specific subjects (drawing figures)
3. Colours series, inspired by my fathers piece "White", which was presumably intended as part of a larger series.











## Checklist



Basic Details

Metamask

		Artist Bio

Portfolio - Socials Links

		Artist CV

		Upcoming Works
# Summary

### Poems
* [Blink](\Poetry\Poems - Blink.md)
* [Fractionation](\Poetry\Poems - Fractionation.md)

### Extended Thoughts
* [Firmament and the Godhead](\Extended Thoughts\Firmament and the Godhead.md)
* [Theories on Thinking about Design](\Extended Thoughts\Theories on Thinking about Design.md)
* [Placement Year Report Draft](\Extended Thoughts\Placement Report Draft.md)
	The Capitalist Manifesto
	* [Contents](\Extended Thoughts\The Capitalist Manifesto\Contents.md)
	* [Introduction - Part 1](\Extended Thoughts\The Capitalist Manifesto\Intro.md)
* [We Need to Talk about the Universities](\Extended Thoughts\We Need to Talk About Universities.md)
* [On Abortion](\Extended Thoughts\On the nature of Abortion.md)
* [Researching via Rhizomatisation (Dota 2)](\Extended Thoughts\Researching via Rhizomatisation - Dota 2 - TI9.md)

### Arty Porjects
* [AI Image Generation, Manipulation, and Upscaling](\Arty Porjects\AI Image Generation and Upscaling.md)
* [Paintings](\Arty Porjects\Painting\Blog.md)
* [Drawings](\Arty Porjects\Drawing\Blog.md)
* [Bookmarks, potentially useful links list](\Arty Porjects\New Document.md)

### Entangled Thoughts
* [Scientism and 'Flat Earthers'](\Entangled Thoughts\Scientists.md)
* [Tetrahedra](\Entangled Thoughts\Tetrahedron.md)
* [Problematic Patenting](\Entangled Thoughts\Problematic Patenting.md)
* [Fragments](\Entangled Thoughts\A Fragment, or A Vision in a Dream.md)
* [Transgendersim](\Entangled Thoughts\Transgendersim.md)
* [The Conscious unconsciousnesses attempt to tyrannise us all](\Entangled Thoughts\The Conscious unconsciousnesses attempt to tyrannise us all)
* [The Hammer and the Sword](\Entangled Thoughts\The Hammer and the Sword.md)
* [Authority, or, What do you want to do?](\Entangled Thoughts\Authority.md)
* [Commercial Design, the Internet, and Progress](\Entangled Thoughts\Commercial Design and the Internet.md)
* [Politics is a False Religion](\Entangled Thoughts\Politics is a false religion.md)
* [The Irish Question](\Entangled Thoughts\The Irish Question.md)
* [Postmodernism, Deleuze and the Diagonam](\Entangled Thoughts\Postmodernism, Deleuze and the Diagonal.md)
* [TWITTER > RSPB and PA](\Entangled Thoughts\RSPB and PA.md)
* [The BBC](\Entangled Thoughts\The BBC.md)
* [Neural-Networkisation](\Entangled Thoughts\Neural-Networkisation.md)
* [Scientism, the Metaphysic, and the Overman](\Entangled Thoughts\Scientism, the Metaphysic, and the Overman.md)
* [Deleuze, Ressentiment and the modern left](\Entangled Thoughts\Deleuze, Ressentiment and the modern left.md)
* [NFTs?](\Entangled Thoughts\NFTS\New Document.md)
* [NFTs?.2](\Entangled Thoughts\NFTS\New Document2.md)

### Glossary / Resources
* [Links](Glossary_Resources\Links.md) 
* [Resources Thoughts](Glossary_Resources\Resources Thoughts.md)
* [Design Noir Notes](\Glossery_Resources\Design Noir Notes.md)
* [Review - The Consumptive View of Self - Rolland Munro](\Glossery_Resources\Review - The Consumptive View of Self - Rolland Munro.md)
* ['Notepad'](Notepad.md)

-------



#Blinks 

Abruptly thunderous raucous abounds.  
Thence did I astart.  

Crashing. And then, 
with only just about a clatter  
bright lightning flashes scream  
loud amongst the clutter.  

Here a whirlpool of air and smoke  
...  
Never-mind, never turn.  
Here I stood, before my window,  
looking out, familiar.   
Perhaps a distraction,  
cloaked by a simple light.  
Oh! but this gaze does tear asunder lands within his sight.  

I see those sunny hills!  
She stares back in all her glory,  
Pastured sills, supporting sinewed rills.  
Gleaming from there amongst, inside   
disdain some will to sustain,  
such stark a lonely tree.  

Before me many rains fall forever.  
He is thunder!   
Bound, kept away, an eternal hunger.  

My eyes fall down,   
Past a now greenless sill.  
Far to the ground, through this ever deepening gill.  
  
There, as the floor rushes toward, a shattering plane of glass,  
and thence, from where, pulsating potted shards glance.    
Though just to see these nightmares?   
  
I awaken aghast amongst a raucous cacophony.  
Still, for some, just a stillness, simply  
In villainous calmness, I did traverse.  
My gaze gently passed.  

Abundantly throughout this place,  
thence I do seem to float away  
above, and abounds within  

The storm clouds return to their trace.  
These illusions fall away, collapsing within, to reform their mortal place  
The cracks slip away, let our one true truce cessate.  

Here I stand, looking out, or through a window, just familiar.  
These hills gaze back through,  
These brilliant blades of light,  
a fractalated manifolding bow.  
Carving gouges by their boiled black rains spite.  

Pieces of a collapsed shattering void.  
Twisting up, all of us,  
and then it just condoles thus  
  
Sucked deep inside this, a version of theirs,  
forever furthering a never coming end,  
the unstoppable sleight.  
 we as ever, descend swiftly deeper,  
an eternal quest for a quickly collated invisible delight.  
  
I place my hand amongst where their windows transfix.  
The glass, a chill.  
Right up to and past my hill, the head *placer*;   
Whence I think, but never quite manage to exist.  
  
My eyes fall past these empty sills.  
A Heaven rains.  
There, I fall back up, through your gills.  

Forever me, my inside world.  
Blinded, not by reflections, but time.  
This one is real,  
This time I am sure.  

Awaken now!  
My struggle hard through their crow's.  
Although many crying clowns lie, ashamed, ignored,  
A spitting wail seemingly endless peals  
ostensibly ignored, now long outcries their shawls  
The bleak sunny morning peers around, there, it is still proud!  

Can I have made it out again?  
Maybe it will become apparent soon  

That gleaming ray of light sees it too.  
My sill shrugs off all its greenery.  

I blink. A mandala in minutia made of words  
Unreads, Unread; A is Under, [drop carefully down] Asunder.  

Further on from abounded raucous,  
amongst a struggle I wept.   
A deep, long, dark tempest.  
Inside I seek something clear,  
Calmly steer throughout within genteel steaming streams.  

I awake, I see.  

Or am I just seeing me..  

- - -


I stare into a dying harmony.
Can this sty sustain the spite-filled armoury.
Should I try to stay forever, 
The scale upon which to measure
leaps unbridled by each individuals view,
points corralled in a' imagined siege.

Intimidation leads to paralysed trepidation, 
false moves swim amongst a probability field of infinite potential, 
are there correct moves, or is there only still the future. 
within uncertainty arises an increasing sense of fractionation.

A marching force pounds as individual will,
send them off to impress the fallow sands.
No one responds to him there, and he cries aloud,
Where was it when the solutions abound?

a cacophony of phonemes crashed down,
passion, emotion, in a syncratic descension.
A magic trick in playful metaphors,
hide the truth in layers of sticks.
# Resources/Thoughts

## ‘Resisting Creativity, Creating the New’. A Deleuzian Perspective on Creativity - Emma L. Jeanes


They make sensible observations, but also implicitly acknowledge the importance of innovation (if companies are buying- in innovation, someone must be innovating) and tend towards a prescriptive approach (arguing that getting the ‘right’ combination of skills to innovate may be rare and difficult still implies there is a recipe for innovative behaviour).  - The University hosts this prescriptive approach, the course is bounded within the department.



* [course element? Free form thinking - group contest - goal oriented ‘product’ - graded by something like:  time taken to ‘do’ the thing? / fewest/highest number of objects? / highest number of interactions? / amount of randomness? / a set of weighted criteria?] (https://www.facebook.com/josephsmachines/videos/999764943534935/



* [pathological consumption has become so normalised that we scarcely notice it](https://www.filmsforaction.org/articles/pathological-consumption-has-become-so-normalised-that-we-scarcely-notice-it/)

* [evolution of design spaces, over prevelance of consumer design - propogation of ‘genes’ through ‘phenotypic effects’ (or the course as a phenotypic expression of consumerised design overprevelance in design teaching)](https://www.youtube.com/watch?v=hYzU-DoEV6k)

* [Can't remember what this is](https://www.youtube.com/watch?v=X0uv5jFaJFk)
* [patents](https://www.youtube.com/watch?v=H4bxzKR7MQs)
* [Yugoslavia - A Concrete Utopia](https://www.youtube.com/watch?v=HEML_VF7Law)
* [patent disputes go mainstream](http://www.ladbible.com/news/technology-china-reportedly-to-ban-sale-of-iphone-models-for-violating-patents-20181210?c=1544462441118&fbclid=IwAR1GhTuXSgZCXTrX2fF1k_ecWUBD8iyeUr_GkEXxzDV1YtdZJZzUWRedqL0)

* [fraud (patents)](https://www.theguardian.com/technology/2018/dec/09/china-threatens-canada-with-grave-consequences-if-huawei-cfo-not-freed?CMP=fb_gu&fbclid=IwAR00tgNrZd-Kt1Sg5npYunDJ_NRcvMYZm01J3-dx738aW_2IlmRB1iI5nDI)

* [blue origins sea landing patent vs tesla suit]() - Find link

* [Design Nature Wiley Sustainable](https://www.amazon.co.uk/Design-Nature-Wiley-Sustainable/dp/047111460X)
* [design activism - (drm)](https://www.dawsonera.com/readonline/9786000015251)

## Jamie Brassett and Betti Marenko

The subtitle to this ‘Introduction’ might well be How to catalyse an encounter between philosophy and design - arguably design is philosophy incarnate, the physical goal of a cognitive creative process, it is the materialisation of pure creation, how more real an encounter than through an exchange of learning between designers, such as in a place of design learning.

We are not, therefore, prescribing a new breed of ‘Deleuzian design’. 
Indeed, is there a Deleuzian way of designing, it is a question worthy of investigation. Providing a prescriptive method for design thinking is a mistaken endeavour, and possibly not education but indoctrination. Therefore the proper way of education should be a widespread exposure to as many different methods for the furthering of the examination and coalescence of an individual's unique pathway towards creative mechanisms.

## Democratic confederalism/confederalism

If the design education system is the backbone of our product market it certainly is the cage of consumer society…… the design education system domesticates the society in the name of capitalism and alienates the designers from their natural processes. 



If the nation-state is the backbone of the capitalist modernity it certainly is the cage of the
natural society …….. The nation-state domesticates the society in the name
of capitalism and alienates the community from its natural foundations”. - Ocalan




### Apple and Designed Obsolecense

[Suppression of information](https://youtu.be/2yJKix17yYE)


### 'Less is More'

[Oki Sato FUNctional](https://youtu.be/c3TPbj2_Xjg)

I think it is something else, it is not 'less is more', it is not about the practical functionality, but the psycho-geographic potential functionality. 'Less physicality, more abstractions'. 

- - -
>    :    Functional use - Semiotics - Semantics    :
>- - -
>    :    Geometry - Beauty - Aesthetics    :    


- - -



[The Bitter Lesson - Compute Reigns Supreme](https://youtu.be/wEgq6sT1uq8)

[Article - The Bitter Lesson - Richard Sutton](http://www.incompleteideas.net/IncIdeas/BitterLesson.html)

> Doing research the classical way of inserting knowledge into a solution is very satisfying - it feels right, it feels like doing research, [and] progressing, and it makes it easy to show in a new paper what exactly the key contributions are.
>However, it may not be the most effective way forward.




["Is Microsoft... the Good Guy?" Xbox adaptive controller, accessibility for everyone](https://youtu.be/MwYTLiXI6zY)
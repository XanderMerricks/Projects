# Links

- - -

### Referencing

[Angelia Ruskin Harvard Referencing Guide](https://libweb.anglia.ac.uk/referencing/harvard.htm)

- - -

### Markdown

[Markdown Syntax](https://sourceforge.net/p/stlviewer/wiki/markdown_syntax/#md_ex_video)
[Video Links](https://stackoverflow.com/questions/14192709/is-it-possible-to-embed-youtube-vimeo-videos-in-markdown-using-a-c-sharp-markdow)

- - -

### Things to make?

[Induction furnace](https://www.youtube.com/watch?v=VTIWcK14tQE)

- - -

### Copyright Law Shenanigans

[We Have Destroyed Copyright Law](https://www.youtube.com/watch?v=BL829Uf2lzI) , and it has had [unintended](https://youtu.be/WPAO_ifX64o) consequences

- - -

### Free Speech

[University Student Suspended for Requested Speech](https://www.youtube.com/watch?v=gA2B7JRDdBU&t=43s)

- - -

### Commodities

[Implications of Commodity Theory for Value Change](https://www.sciencedirect.com/science/article/pii/B9781483230719500167)

- - -

### Painting

[Illusion of Detail](https://www.youtube.com/watch?v=5jffJeeXaVQ)

- - -

### Animation

[Doing the Walk 1](https://www.youtube.com/watch?v=09K2Dzli_3o)

[Doing the Walk 2](https://www.youtube.com/watch?v=NW268myd798&t=14s)

[Cartoon head process](https://www.youtube.com/watch?v=1C3kW9tM4k4)

[On 2D Animation - Jason Shum](https://www.youtube.com/watch?v=owlaxXNFYHs)

[Mike Polvani Animation Lecture at AI-OC Part II](https://www.youtube.com/watch?v=tkKvVcr5gqg)

[12 Principles of Animation](https://www.youtube.com/watch?v=uDqjIdI4bF4)

- - -

### Film

[Arrival: A response to bad movies](https://youtu.be/z18LY6NME1s)

[Julian Schnabel -director of At Eternity's Gate- on Vincent van Gogh's undying appeal](https://www.youtube.com/watch?v=9gJeMG1p5aQ)

- - -

### Technologies

* 2019

[3D Printing - Light](https://youtu.be/Yy-d5VVZlxQ)

[Nuclear Batteries - Carbon-14 Diamond Battery](https://youtu.be/Fj7z8wFGzDE)

[The Bitter Lesson - Compute Reigns Supreme](https://youtu.be/wEgq6sT1uq8)

* Compliant Hinges

[Flexible Machines](https://youtu.be/97t7Xj_iBv0)

* Computers

[Inside i7-8700k - Scanning Electron Microscope](https://www.youtube.com/watch?v=O_iu48VTRDE)

- - -

### Camera with LEDs

[Camera Eyes](https://www.youtube.com/watch?v=bDaELRvL3WA)

- - -

### Philosophy - Thought

[Walter Benjamin: Art in the age of mechanical reproduction](https://www.youtube.com/watch?v=blq9sCIyXgA)

[A Car With Visual Lag](https://www.youtube.com/watch?v=7ZK_fnS62Lk)

[Regression to the Mean](https://www.youtube.com/watch?v=1tSqSMOyNFE)

[Why do people think what they think](https://www.youtube.com/watch?v=Lae9K2Sp6p4)

[Google Staida in five minutes](https://www.youtube.com/watch?v=4SOS-a4ks7s)

[Where did Homo Sapiens really come from? - Homo Naledi](https://www.youtube.com/watch?v=xv4_L5zlYaA)

[Steven Pinker: Linguistics as a Window to Understanding the Brain](https://www.youtube.com/watch?v=Q-B_ONJIEcE)

[Apple - Suppression of information](https://youtu.be/2yJKix17yYE)

[Face to Face | Carl Gustav Jung (1959)](https://youtu.be/2AMu-G51yTY)

[Pronunciation](Pronunciation.md)

### Podcasts

[Guy Ritchie on Joe Rogan](https://youtu.be/CMejeTP2edI)

[Joe Rogan Experience #1284 - Graham Hancock](https://www.youtube.com/watch?v=Rxmw9eizOAo)


- - -
- - -
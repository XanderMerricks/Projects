# Pronunciation Key

### English Sounds

>ɑ:	 |	as in father ('fɑ:ðə), alms (ɑ:mz), clerk (klɑ:k), heart (hɑ:t), sergeant ('sɑ:dʒənt)
>- - - 
>æ	 |	as in act (ækt), Caedmon ('kædmən), plait (plæt)
>- - -
>aɪ	 |	as in dive (daɪv), aisle (aɪl), guy (gaɪ), might (maɪt), rye (raɪ)
>- - -
>aɪə	| 	as in fire (faɪə), buyer ('baɪə), liar ('laɪə), tyre ('taɪə)
>- - -
>aʊ	 |	as in out (aʊt), bought (baʊ), crowd (kraʊd), slouch (slaʊtʃ)
>- - -
>aʊə	 |	as in flour ('flaʊə), cower ('kaʊə), flower ('flaʊə), sour ('saʊə)
>- - -
>ɛ	 |	as in bet (bɛt), bury ('bɛrɪ), heifer ('hɛfə), said (sɛd), says (sɛz)
>- - -
>eɪ	 |	as in paid (peɪd), day (deɪ), deign (deɪn), gauge (geɪdʒ)
>- - -
>ɛə	 |	as in bear (bɛə), dare (dɛə), prayer (prɛə), stairs (stɛəz), where (wɛə)
>- - -
>g	 |	as in get (gɛt), give (gɪv), ghoul (gu:l), guard (gɑ:d), examine (ɪg'zæmɪn)
>- - -
>ɪ	 |	as in pretty ('prɪtɪ), build (bɪld), busy ('bɪzɪ), nymph (nɪmf), pocket >('pɒkɪt), sieve ('sɪv), women ('wɪmɪn)
>- - -
>i:	 |	as in see (si:), aesthete ('i:sθi:t), evil ('i:vəl), magazine (ˌmægə'zi:n), receive (ri'si:v), siege (si:dʒ)
>- - -
>ɪə	 |	as in fear (fɪə), beer (bɪə), mere (mɪə), tier (tɪə)
>- - -
>j	 |	as in yes (jɛs), onion ('ʌnjən), vignette (vɪ'njɛt)
>- - -
>ɒ	 |	as in pot (pɒt), botch (bɒtʃ), sorry ('sɒrɪ)
>- - -
>əʊ	 |	as in note (nəʊt), beau (bəʊ), dough (dəʊ), hoe (həʊ), slow (sləʊ), yeoman >('jəʊmən)
>- - -
>ɔ:	 |	as in thaw (θɔ:), broad (brɔ:d), drawer ('dɔ:ə), fault (fɔ:lt), halt (hɔ:lt), organ ('ɔ:gən)
>- - -
>ɔɪ	 |	as in void (vɔɪd), boy (bɔɪ), destroy (dɪ'strɔɪ)
>- - -
>ʊ	 |	as in pull (pʊl), good (gʊd), should (ʃʊd), woman ('wʊmən)
>- - -
>u:	 |	as in zoo (zu:), do (du:), queue (kju:), shoe (ʃu:), spew (spju:), true (tru:), you (ju:)
>- - -
>ʊə	 |	as in poor (pʊə), skewer (skjʊə), sure (ʃʊə)
>- - -
>ə	 |	as in potter ('pɒtə), alone (ə'ləʊn), furious ('fjʊərɪəs), nation ('neɪʃən), the (ðə)
>- - -
>ɜ:	 |	as in fern (fɜ:n), burn (bɜ:n), fir (fɜ:), learn (lɜ:n), term (tɜ:m)
>- - -
>ʌ	 |	as in cut (cʌt), flood (flʌd), rough (rʌf), son (sʌn)
>- - -
>ʃ	 |	as in ship (ʃɪp), election (ɪ'lɛkʃən), machine (mə'ʃi:n), mission ('mɪʃən), pressure ('prɛʃə), schedule ('ʃɛdju:l), sugar ('ʃʊgə)
>- - -
>ʒ	 |	as in treasure ('trɛʒə), azure ('æʒə), closure ('kləʊʒə), evasion (ɪ'veɪʒən)
>- - -
>tʃ	 |	as in chew (tʃu:), nature ('neɪtʃə)
>- - -
>dʒ	 |	as in jaw (dʒɔ:), adjective ('ædʒɪktɪv), lodge (lɒdʒ), soldier ('səʊldʒə), usage ('ju:sɪdʒ)
>- - -
>θ	 |	as in thin (θɪn), strenght (strɛŋθ), three (θri:)
>- - -
>ð	 |	as in these (ði:z), bathe (beɪð), lather ('lɑ:ðə)
>- - -
>ŋ	 |	as in sing (sɪn), finger ('fɪngə), sling (slɪn)
>- - -
>ə	 |	indicates that the following consonant(l or n) is syllabic, as in bundle ('bʌndəl), button ('bʌtən)
>- - -
>x	 |	as in Scottish loch (lɒx)
>- - -
>əɪ	 |	as in Scottish aye (əɪ), bile (bəɪl), byke (bəɪk)

## Length
The symbol : denotes length and is shown together with certain vowel symbols when the vowels are typically long.
- - -

## Stress
Three grades of stress are shown in the transcriptions by the presence or absence of marks placed immediately before the afected syllable. Primary or strong stress is shown by ', while secondary or weak stress is shown by ˌ. Unstressed syllables are not marked. In photographic (ˌfəʊtə'græfɪk), for example, the first syllable carries secondary stress and the third primary stress, while the second and fourth are unstressed.
- - -
- - -
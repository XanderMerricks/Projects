“They seek out (ab)user friendly products that lend themselves to imaginative possibilities of this parallel world of illicit pleasures stolen from commodified experience” P7

“Consumers as anti-heroes: some cautionary tales” P7 (stories about people abusing technology to cause harm - Cyberbullying leading to suicide - A man marrying his TV “These individuals are not rejecting other people because of technology; they have found happiness with technology instead. Before the advent of television and the web, they might have been lonely”)

02: hertzian space
Nowhere to hide
Intense militarisation has lead to nowhere to hide, intense surveillance and weaponisation of the spectrum. 

	“Unsurprisingly, industry views hertzian space solely as something to be bought and sold and commercially developed for use in broadcasting and telecommunications”
“Unauthorised use is viewed as trespassing”
Discusses other governmental uses of hertzian space
(“Electromagnetism is life! And death…… spectres of the system 1999 - Craig Baldwin)

“Objects not only ‘dematerialise’ metaphorically, in response to miniaturisation and replacement by services, but literally dematerialise into radiation” p12

“As designers we are interested in the interaction between devices, hertzian space and the imagination.” p15

Spectral geographies
“Hertzian space sits within the physical realm” - it's very real 

Physical space affects hertzian space, and is monitorable in a similar way too. There are spaces designed to not have any presence in hertzian space ie “US West Virginia National Radio Quite Zone [sic] “ p20

Electrosmog
A new form of pollution - example: Vatican radio broadcasting leading to high incidence of leukemia; and electro-sensitive people
Development of protective products to combat this new ‘threat’

Radiant objects
Without entering a premises, someone could use the hertzian signals given off to map possible internal activities. 
Even computer screen readouts
Realisation that computers leak 
TV detector vans
When has a law been broken
Ghostliness has assumed itself into unexpected incidents from electromagnetic interference, the idea that ghosts are a person's disturbance in the hertzian realm. 

Immaterial sensuality 
“The challenge today is not to create electronic space but electronic-free space.”  p26
“using a jammer constitutes some form of trespass.” p27
Hertzian space has material props to keep it going - led to disguising of these props ie false trees / hidden inside statues

Connoisseurs 
People who have made games, or derive pleasure from the electromagnetic, ie “foxhunting”
People gain an understanding or a view of hertzian space the more they interact with it 
‘whistler hunters’ (natural radio) 
Pylons being a staple view, but largely ignored (hyper normalisation?) 

Electro-sensitives
Allergy to electromagnetic fields
People develop diy treatments (is it psychosomatic) 
“we are not recommending that designers try to predict the misuses of products, but rather that they refer to this rich narrative space as a context of use instead of the models of normality usually referred to when new functional possibilities are being developed. Designers could draw on the specialist knowledge, concerns and pleasures of beta-testers, early adopters, electro-connoisseurs and hypersensitives to evolve a deeper understanding of how to make ourselves at home in this new environment” P40





Product genres
There could be so many genres of product beyond the bland Hollywood mainstream: arthouse, porn, romance, horror - noir, even- that exploit the unique and exciting functional and aesthetic potential of electronic technology. Although many products already fall into genres - Alessi products attempt design as comedy, designs for weapons and medical equipment can shock and horrify, sex-aids are obviously a form of design porn and white goods express a wholesome and romantic idea of settled domesticity - they do not aesthetically challenge or disturb. 
P45 design noir


Today, designs main purpose is still to provide new products - smaller, faster, different, better. P58


“To be considered successful in the marketplace, design has to sell in large numbers, therefore it has to be popular. Critical design can never be truly popular, and that is its fundamental problem. Objects that are critical of industry's agenda are unlikely to be funded by the industry. As a result, they will tend to remain one-off. Maybe we need a new category to replace the avant-garde: (un)popular design.”  P59 -  “A world where shopping has more political impact than voting is a threat to democracy.”
Instead of thinking about appearance, user-friendliness or corporate identity, industrial designers could develop design proposals that are both critical and kptomisfic, that engage and challenge 

Ippei Matsumoto - Life Counter (2001) “the counter is designed to be visually unassuming and could easily fit into the slightly retro-futuristic style of the moment. It is a classic noir product, it's power lies in its precise function and low key display of disturbing information.” P63


P64 4th paragraph - they are products for the mind… If they look as if they really should be used - objects like these can quickly become ridiculous. 




“I love the fact that it is ironing cable - like an appliance. I wouldn't have liked it so much if it was all plastic coating.” (Emma and Constance P3) 










Design Noir is a book written by Anthony Dunne and Fiona Raby, published in 2001.

It is broken down into five sections under, DIY Realities, Hertzian Spaces, Design Noir, Designer as Author, and The Secret Life of Electronic Objects. It examines the idea that electronic products occupy a physical space and also a Hertzian space. They discuss the importance of this hertzian space, and how far people view electronic products as pervading the space we inhabit.

DIY Realities is an analysis of the Users themselves, and what they want and need from products. They discuss user's’ abilities in subverting the use of an object or “cheating the system and deriving more pleasure than is due.” They discuss the “unpredictable potential of human beings to establish new situations despite the constraints on Everyday life imposed through electronic objects.” This section ends by introducing the idea of Hertzian space under the idea “When objects dream…”. It is the idea that electronic products “leak radiation into the space and objects surrounding them” and how we interact with this invisible space which is constantly manipulated by the electromagnetic fields created by electronic products.
The idea that if we could ‘see’ this Hertzian space objects would “literally materialise into radiation”




# The process of rhizomatic researching.

I have been watching *The International* tournament of Dota 2 in Shanghai over the past week. This has meant tearing up any sense of a sensible sleeping pattern or any sense of sense inside my waking mind. It has been a fairly brain dead caffeine fuelled week of Dota, punctuated by spiralling thoughts about the design and situation of the strata of the game as an object.

To contextualise this thought before I expand any further into it; it only seems sensible to start with Valve - the game's proprietor and propagator - and Dota as a game in its own right. Valve is a games platform, market place, and product manufacturer; the expanding complexification of businesses and non-person entities is surely effecting the process outcomes - whatever they may be; whether product based or elsewhere - of these entities over time.

Given Valve as an ecosystem rather than business and Dota as an organism rather than game within that ecosystem, it might be possible to extrapolate what effects may be being imposed upon the use based outcomes of the products produced, what that might mean for the future trajectories of ecosystems and products; and those people that could use them.

Dota 2 is a video game, a fairly simple product in reality, except in the traditional sense, it is anything but simple. The product should cost nothing in monetary terms to its user, however the game makes a significant amount of money for Valve every year. It is an incredibly complex system with a very steep learning curve for participation, yet, at the very moment I am writing this there are 430,901 players in game now, and 10,374,236 unique players in the last month, whilst there are as I type 15 players left in *The International*a tournament with a total prize pool of $34,221,596.

I think that should do as an over view potentially, if not, google's always there. 

Given the size of the game at the moment, and the clear benefits increasing the size of the game to Valve's bottom line should be. It is excedingly confusing to me as to why the streams and presentation of their biggest tournament (presumably a significant moment for the company) is so poor, and seemingly designed to be as confusing and obfuscated as possible. 
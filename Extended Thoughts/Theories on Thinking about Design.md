# Theories on Thinking about Design

* Design Thinking Theories
* The Rhizome
* Deconstructionalism

## Introduction 

“It was a lovely spring morning - may 22nd, to be exact - when we made that fateful journey which brought me on to a stage which is destined to be historical.” (Conan Doyle 1995: 446-447)



a précis of activities and experiences - an adventure map of some kind?


A plateau, a middle, not at the beginning or the end. The same Professor Challenger who gave a lecture to quite a crowd, in the mind of Two Among Several. There explained the body without organs within and without, he leads a discussion through the strata of the Earth and the history with a Geology of Morality. So as to situate a quote from an earlier conversation,  

>“A book has neither object or subject; it is made of variously formed matters, and very different dates and speeds. To attribute the book to a subject is to overlook this working of matters, and the exteriority of their relation. It is to invent a beneficent God to explain geological movements. In a book, as in all things, there are lines of articulation or segmentarity, strata and territories; but also lines of flight, movements measured in terms of deterritorialization and destratification. ”  

They go on at great lengths following lines such as; “A book is an assemblage” | “It is a multiplicity” | “it also has a side facing a body without organs” | They then require a lengthy discourse on the nature of the rhizome. A Journey  through; connection and heterogeneity, multiplicity, and asignifying rupture. 
By way of an aside, and a sifting through their planes of deconstruction, they move at various speeds through passages on the relative nature of movements in deterritorialization, and thereby through a processes of reterritorialization; to create the book as a rhizome within the world.  

To them “Mimicry is a very bad concept [however], since it relies on binary logic to describe phenomena of an entirely different nature”; and even despite their self deconstructing view on the topic itself however, this only serves to be an overly protectionist plateau to emanate from such a disintegrating machine; so I’ll move on. However it comes, these ideas are inserted before a continuation through an undulating continuation on cartography and decalcomania. Just to describe what a rhizome is, by having described what it is not.

The continue; onwards! Without so much as a care as to whether the reader is following, continuing on to further deconstructions ultimately finding at the bottom a final platform under which they had found themselves stood, so as to solve the problem of having found it; deconstructed that as well. 

To finally come to describe the broken rhizome. To begin by way of describing Why? and then, to come to a method, How? They then float amongst many arborescent trees only to decide that they clearly must have had their time. To thus conclude a rhizome is not that, and must therefore have no beginnings or ends; it is always in the middle, between things, interbeing.^1. They paused momentarily as I looked elsewhere to consolidate a transition from arborescense to a more rhizomatical connection, this required a rumination elsewhere however, so I turned away from the mind of Two among Several. Back in a reality, 

## To Generate a Playground for Ideas


There are so many styles of writing, and methods by which to create an adequate word picture to transmit ideas and potentiality through space and time. Post-modernism, or the many schools of thought collected under the label, claim that there is no distinct objective observable reality, and as such; nothing should be treated to be such, and claims of such are to be disregarded. There is something about the rhizomatic generation of an arborescently connected thought map that link and grow ideas out of 'fundamental truths'. 

For example, "Peterson (2014) claims that it is necessary to add a reference after a sentence containing an opinion which is not your own, or a fact that you have acquired from some source material." Would that be necessary in this case, given that he's referenced himself and the essay within itself. Or given the nature of this papers grounding within Deleuzian processes would it be necessary to give a comprehensive list of referable material as and when to give an escalator-esque ascent through the strata's? Everything is connected to everything else, all of the time, all books are in conversation (competition) with each other and themselves.

The limitless nature of interconnections between people leads their individual production into situations whereby "books have a way of influencing each other". With the ever increasing weight of the historical library, could the act of 'plagiarising', and thinking, or writing become analogous? When does the quantity of content available become too great such that everything has influenced everything to the point such that referencing is obsolete or absurd (LoB: xz,txvms90). 

![10 to the 29 exact matches](10 to the 29 exact matches.png)

Deleuze, Guattari; developed and constructed an organising thought, of reference, *and content*. These ideas have surreptitiously permeated through the collective consciousness ('cul-tur-') much like the Marxian thoughts of the previous century.

A designed deliberate subterfuge however, I find unlikely. As Woolf collates; "nobody could put the point more plainly. 'The poor poet has not in these days, nor has had for two hundred years, a dog's chance...a poor child in England has little more hope than had the son of an Athenian slave to be emancipated into that intellectual freedom of which great writings are born.'" (Woolf 2014) Does the relevance and connection through the nesting of quotations shine through when not shown to the reader through the explicature of the wording and citation; to what extent does it matter? Or are there always too many lines of flight. *Q*? 

The stratification of this, and every stratarised (parametrised) society, obfuscates, and discriminates the plights (flights) of the people in respect to the contextual connections and hierarchical relationships between their planes of reference.
This appears to have had a widely-roaming impact; in preventing a large swathe of society from interacting with complex ideas, design, or literature. The 'poor' (think in all terms except pecuniary, before you think monetarily) as a section of society is growing, extracting more people from the pool of potentially productive thinkers, designers, and creators.

The idea that people shouldn't or "don't read" leads to the conclusion that people *shouldn't* or *don't* think. But *thinking* is being, creating, designing. The process of the decoupling of the thinker from the creator is destructive. It is the same process by which commodified capitalism attempts to strip the producer from the consumer, and the consumer from the producer. To stratify the *'creator'* into an assemblage of its *Users*, it is an attempt to remove the individual from the producer. The demolition of the individual within the creation and development of ideas, concepts, and objects renders them functionless or at least incomplete within the externalities of their designed environment.

The goal of production being as an attempt to complete a set of the stratification containing the combinatronical sets of the base functional uses of object-things. 






## To Spread Outwards by Moving Inwards Towards the Self

On the consumptive view of self by way of extension, exchange, and identity; Rolland Munro calls forth a post-modern model to analyse the self as a site of consumption by way of “a going out from self to the social, and then returning.” He recalls a line of Friedman emphasising for himself “that goods are building blocks of life-worlds”3. The developing line of thought diverges along two paths what is and what should be done with that. The lines of ‘what is?’ thought travel to sets of questions which are “of course, partly [of] a descriptive [nature], one that has us running to the texts, either to argue over interpretations of terms or disputed sources as requisite authorities.” 
The post-modern and post-structuralist idea suggests a movement or journey from storylines and lines of flight from conception to deterritorialization and a dismantling. The crashing of Marx within a rhizomatic field of abstracted value and deconstructed purpose allows a cunning thought: whereby the accumulated assumptions become “implicit in economic theories, where an exchange value of goods can be interpreted as a deferral of the use value” such that the initial exchange value of a given good augments its use value in the mind of the consumer such that the idea that “use value and exchange value therefore move hand in hand” would move to become “to seem arbitrary and broken.” Whereby the value of exchange, and by close association, the value of use, “goes all the way down, but crucially, has to be fabricated at every point”. The consumption of the identity of a person by their use value in relation to their potential exchange value is brought to bare in the self-referential calling of function as action. As in ‘What do you do?’
This is exemplified I think nicely, in a manner of the word that does not imply niceness, by way of the naming persons by their function rather than identity, for example, ‘the hired gun’ or “the manual workers who travelled to the factory [who] became known as ‘hands’”(Munro 1996). 
The impressionists and situationists of the early 20th century split identity and self, so as to view themselves from different perspectives as beings of belonging. The ‘Self’ watches outwards throughout the eyes of the ‘Identity’ into the world; an ‘Identity’ tends in the reverse. The view of the world as ‘building blocks’ to be assembled deflates and devalues the identity of self as it “insinuates a ‘Lego-self’.” This would seem to be a mistake to me however, the ‘lego-self’ among the ‘building blocks’ of the world must be assembled from the outside in rather than the inside out, the process of return from the exterior to interior allows the collection of knowledge from peripheries. A managed construction by assemblage will generate an arborescent multiplicity of patterned interconnectivity. This planned creation will be incapable of divergence from hegemonic thought by way of critique through a backward-facing or translational analysis.
On consumption and commodities the importance of Munro’s foray into the consumptive view of self is evident. He lays bare ‘the how’ and ‘Why’, the idea that a constant process of ‘extension’ without a method of cyclic ‘retraction’, becomes self defeating as a method for consolidating knowledge, a masquerading fallacy. He concludes, powerfully, outwardly reaching towards the people of the postmodern.

>“Postmodern Writing, much of it in the guise of theorising consumption, has begun to from its own ‘nega-narratives’, hollowing out positions that rail against those who, for example, hold onto unique or self centred notions of self. Asleep in a hammock of their own incredulity, these ‘nega-narratives’ delete debate over self. For the moment, the membership stakes over postmodernism seems to make its writers oblivious to recent arguments, like those of Cohen (1994). Of course Cohen, in re-mobilising a notion like that of ‘self-consciousness’, still animates self at its autonomous and anonymous core. But he is still able to point to the vacuity of current assumptions about self, in a way that traces these to the modernist heritage of what passes for knowledge in anthropology and sociology.
Is there no alternative, then, but that of either centring or de-centring the subject?” 

By way of an addendum and finalisation to the ideas of the expansion of the decentered self he collects three points:

“First, as persons we are always in extension”.

“Second, there are only partial connections” - attachment is detachment, and detachment, attachment; it is a journey around the surplus rim of self. 

“Third, is a matter of scale” - diminishment against magnification, “one includes what the other ‘cuts out’”. 

With a final breath he harmonises the differences and disagreements between the central themes and personas presenting them by way of flipping the coin to show that both sides are attached, but invisible to each other. Quite simple really, *magic*.

The complications arise however when analysing the probability of any argument being a single side (face) of a coin. For example, is it possible to know when the idea or central thought is at the centre of one of the sides (faces) of the coin, in the middle, or on the edge - this area of complication leads further however. Is it possible to know which side (face) of the argument the idea sits. Or further whether it sits on a side (face) at all, or whether the idea has slipped off of the edge of the side (face) itself and onto the side (edge) of the sphere or discourse itself. 
If indeed it is possible to ‘know’ - or more accurately ‘to guess/to find out’ - which side (face) of the argument the idea finds itself, or whether it is on a side (face) at all and in fact has slipped unnoticed into the area (edge) within.

If the coin as the argument lands on its edge both sides can be seen clearly as the information that they represent - joined together in union by the ideas which have fallen within. However if the coin lands either of the ways up - which is significantly more likely - only one side (face) of the argument is visible, hiding an equal portion of the coin from view, but imply that the ideas which have fallen within connect the arguments side (face) to the very ground which supports it - or viewed another way, as all of the ideas which have fallen within support the side (face) as presented by the world. This dichotomy of sides (faces) as joined by another different side (edge) is masked when the coin lands on one face rather than the edge. Obscuring the ‘real’ nature of the debate/argument behind the presentations of one of the sides (faces) as supported by all then ideas fallen within, rather than by implicit connection to the hidden side.





## References - Bibliography

Gilles Deleuze and Felix Guattari-Plateaus 1 and 3. ISBN: 978-1-7809-3537-9

(LoB: xz,txvms90) - https://www.libraryofbabel.info/bookmark.cgi?xz,txvms90

Munro, Rolland (1996). The consumptive view of self: extension, exchange, and identity, *Consumption Matters*, 248-273.

Peterson, J.B. (2014). Essay writing for writers. Journal of Essay Writing, 01, 15-24.

Woolf, Virginia (FP 1928 : 2014). A Room of One's Own, 01-138.
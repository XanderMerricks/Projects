# Introduction

* "why Design?"
* How to spread time
* an analysis of an industry standard placement
* a rhizomatic multiplicity on the subject of "Why Design?"

- - - 


I began my year before; 

This time is set aside within the confines of the course as laid out by the 'placement' for the purpose of gaining the 'Professional Experience' addendum to the title of an eventual degree thereby completed.

It is posed as a time for the development of the internal designer through the exposure to processes, tooling, and methodology; an experience - in a manner unattainable within the confines of the institutions course.

The expectation from the institutions perspective is that this will function as a pre-constructed *supported* stepping-stone from which they can advance their desired progression, into a future career, within their preferred speciality.  

However I see a state whereby the systems of production design is beginning to crumble the structural system supporting the general strata within, it is beginning to collapse under the weight of an attempt to convert a finite commoditised materialistic model into the infinitum as represented by the digital world - the Internet, the I-O-T, (...).  

An inevitable disintegration is being put off by an attempted eternal perpetuation of the current state of affairs. A closed consumer based design sphere propped up with an educational system as directed by a cultural view as 'presented' by the commodified capitalising markets; and their agents - advertising.  

A Summary..  

This is a minor précis of the events of the past year - a ‘placement’ year. This is a complex subject to broach, and therefore difficult to process a starting point from which to initiate a discussion.  

This document was initially presented, by the university, to be an eight thousand word report on the experiences collected throughout the year.  

I had experienced another side to the distribution of knowledge and thereby the process of learning, than that presented by the universities course. This is presented in the form of the qualification structure of the FabAcademy, the distributed lab network of Fablabs, the social network and production networks powered by git and fablabs.io.  

This informed my decision and acted as a centralisation to the expansion and subsequent development of my thinking regarding the future of design and my place within it as a designer.  

I had planned to act as a supervisor and advisor within the fablab as funded by the university, with the addendum of partaking within the fabacademy program for the year.   

Before the academic yearly cycle had truly began it became clear that the control systems and personnel of the placement structure was going to change without the content structure being changed. The situation with regards to the future and longevity of the fablab was also becoming unstable.  

The transition of coordinators, amongst the confusion and chaos of the beginning processes of the year damaged, and subsequently led to the collapse of my placement as cleared by the university system.  

I had throughout this time been considering the possible directions for the future of production, manufacture, and processes of commodification; and within that frame how I could fit in as a designer amongst  structures.  

There is no simple mechanism by which to describe the nature in which this year has led to the development of my ideas and thought processes, and subsequently the conclusions I have made from this time.  

The following is a summarisation of my thinking and yearly output as represented within the lens of the course structure. Research, Ideation, Development, Launch, and Contextual (Historical).  


# Research



I cannot tell whether I have had an epiphany, but everything below is probably better to be sunk at this point.



[Petrosomatoglyph](https://en.wikipedia.org/wiki/Petrosomatoglyph) - these seem interesting.





- - -
- - -


When the designer comes to researching a question - or a problem, they scour deep their repository of knowledge in an attempt to call up pre-generated solutions to un-thought problems. This information is utilised to solve a problem such that it generates in an amorphic manner; such that the trains of thought have no beginning or end; such that a rhizomesque-abstraction of the superstructure of knowledge can manifest; the relevant information which emerges itself in-turn becomes the answer(s).

Any question can be represented by a tree of further questions, a further set of questions. This set of questions at first can seem benign or arbitrary. However, for example, a question of issue, or problem, can be represented simply; 

> Why does this problem exist?

 or taken as a more complicated question about the nature of the sustenance of the problem;

> Why has no one else thought to think of this as a problem such that it has sustained its problematicness till it was observed by someone such as me?

 or taken as a more complicated question about the nature of the set of skills of the previous 'problem' observers;

> Why has no else, like I have, considered that their skills, and their alone, could be sufficient to solve that problem such that it would not be a problem here for me to fix right now?


They present slightly different questions and therefore extremely different solutions, or goal-based outcomes. This can be demonstrated by breaking the question down into its more simple constituent parts. 

With respect to the first question "Why does this problem exist?" we are asking a basic set of questions.

> Who is this a problem for?
> - - -
> What is the problem?
> - - -
> Where is it a problem?
> - - -
> When is it a problem?
> - - -
> Why is it not a problem?

This is a complete set of who, what, where, when, why questions. 

To effectively solve a problem the designer first needs to assess whether the problem really exists at all, or whether he simply stands in the position whereby the scenario appears to present a problematic situation despite it being a trade-off of its comparative benefits against its level of 'negative' effects.

The central failure and problem with any Problematic Situational Scenario is that they appear to require as solutions an increasingly higher order product or system - furthering expanding perceived and actual complexity, and systemic fragility - to solve not only the functional problems which arise from Problematic Situations, but also with respect to the stability of the problems situational scenario. This ever expanding idea that problems should be solved when found has led to an obsession with ever more functional objects designed to bridge the gap in functionality caused by the transition from problem causer to problem solver. The only issue, potentially significantly of this methodology is the requiring of a problem solving observer to observe and solve problems which they themselves cannot see. 

Take, for another example, the idea of Chesterton's fence:

> The quotation is from G. K. Chesterton's 1929 book *The Thing*, in the chapter entitled "The Drift from Domesticity":
> 
> In the matter of reforming things, as distinct from deforming them, there is one plain and simple principle; a principle which will probably be called a paradox. There exists in such a case a certain institution or law; let us say, for the sake of simplicity, a fence or gate erected across a road. The more modern type of reformer goes gaily up to it and says, "I don't see the use of this; let us clear it away." To which the more intelligent type of reformer will do well to answer: "If you don't see the use of it, I certainly won't let you clear it away. Go away and think. Then, when you can come back and tell me that you do see the use of it, I may allow you to destroy it."[1]
> 
> Chesterton's admonition should first be understood within his own historical context, as a response to certain socialists and reformers of his time (e.g. George Bernard Shaw).
> [Wikipedia - Chesterton's Fence](https://en.wikipedia.org/wiki/Wikipedia:Chesterton%27s_fence)

- - -

(WIP)------ > 

Such that in any given state the functional apparatus of an object may appear to be contrary to it's ability to functionally consist; based simply on the observational state of the situation. Given this significant limitation on the observational designer. Should the role therefore transition *not* simply from creating products that do things that other products do not, to a hyper-personalised object-sphere which preform specific functions based on the emergent function-system within which they grow to exist; but instead to a world in which users are their own designers based on their functional experience of living amongst a highly objectified functional world, which in turn would allow higher-order designers to become concerned with the solving of systematically fundamental problems ingrained within our eco-systems; rather than the generation of an object based material reality, which has felt an increasingly rapid shift out of materiality into an immaterial world posing as reality.




- - -
- - -



# Ideation



On plagiarism - Relationship to Library of Babel Point

...yeah so basically you give it you start a text oh we can say Joe Rogan experience is the greatest podcast ever and then let it finish the rest you know start explaining stuff about why it's the greatest podcast is it accurate oh wow look at this it says a move that threatens to push many of our most talented young brains out of the country not to campuses in the developing world this is a particularly costly blow research by Oxford University warns that the UK would have to spend nearly 11 1 trillion on post brexit infrastructure that's crazy that that's all done by an AI yeah that's like spelling this out in this very convincing argument the thing is the act the way it actually works algorithmic is fascinating because the generate is generating it one character at a time it has as far you know you don't want to discriminate against AI but as far as we understand it doesn't have any understanding of well still of what it's doing there any ideas it's expressing it's simply stealing idea it's like the largest-scale plagiarize er of all time right it's basically just pulling out ideas from elsewhere in an automated way and the question is you could argue us humans are exactly that we're just really good plagiarize errs of what our parents taught us of what our previous so on yeah we aren't for sure yeah so the question is whether you hold that back their decision was to say let's hold it let's not release it that scares me



# Development


[Make things that people will be nostalgic about](https://youtu.be/juRkaqkDfCM)

["Is Microsoft... the Good Guy?" Xbox adaptive controller, accessibility for everyone](https://youtu.be/MwYTLiXI6zY)


There are so many styles of writing, and methods by which to create an adequate word picture to transmit ideas and potentiality through space and time. Post-modernism, or the many schools of thought collected under the label, claim that there is no distinct objective observable reality, and as such; nothing should be treated to be such. There is something about the rhizomatic generation of arborescently connected thoughts that link and grow ideas out of 'fundamental truths'. "Peterson (2014) claims that it is necessary to add a reference after a sentence containing an opinion which is not your own, or a fact that you have acquired from some source material." Would that be necessary in this case, given that he's referenced himself and the essay within itself. 

The limitless nature of interconnections between people leads their individual production into situations in which "books have a way of influencing each other" (Woolf 2014). With the ever increasing weight of the historical library could the act of 'plagiarising' and thinking or writing become analogous? When does the quantity of content available become too great such that everything has influenced everything to the point such that referencing is obsolete or absurd. Deleuze, Guattari; developed and constructed a 'new' method of organising thought, reference, and content. These ideas have surreptitiously permeated through the collective consciousness ('cul-tur-') much like the Marxian thoughts of the previous century.

Deliberate subterfuge however, I find unlikely, as Woolf collates; "nobody could put the point more plainly. 'The poor poet has not in these days, nor has had for two hundred years, a dog's chance...a poor child in England has little more hope than had the son of an Athenian slave to be emancipated into that intellectual freedom of which great writings are born.'" (Woolf 2014) Does the relevance and connection through the nesting of quotations shine through when not shown to the reader through the explicature of the wording and citation; to what extent does it matter? Or are there always too many lines of flight. *Q*? 

The stratification of this, and every stratarised (parametrised) society, obfuscates, and discriminates the plights (flights) of the people in respect to the contextual connections and hierarchical relationships between their planes of reference.
This appears to have had a widely-roaming impact; in preventing a large swathe of society from interacting with complex ideas, design, or literature. The 'poor' (think in all terms except pecuniary, before you think monetarily) as a section of society is growing, extracting more people from the pool of potentially productive thinkers, designers, and creators.

The idea that people shouldn't or "don't read" leads to the conclusion that people *shouldn't* or *don't* think. But *thinking* is being, creating, designing. The process of the decoupling of the thinker from the creator is destructive. It is the same process by which commodified capitalism attempts to strip the producer from the consumer, and the consumer from the producer. To stratify the *'creator'* into an assemblage of its *Users*, it is an attempt to remove the individual from the producer. The demolition of the individual within the creation and development of ideas, concepts, and objects renders them functionless or at least incomplete within the externalities of their designed environment.

The goal of production being as an attempt to complete a set of the stratification containing the combinatronical sets of the base functional uses of object-things. 




Peterson, J.B. (2014). Essay writing for writers. Journal of Essay Writing, 01, 15-24.
Woolf, Virginia (FP 1928 : 2014). A Room of One's Own, 01-138.



- - -
- - -


Furthering thoughts ^ I am not sure the above will work any more.

['Millennial consumption' Peter Zeihan](https://youtu.be/BHr999RGPQw)

[Transcript](..\Glossary_Resources\Peter Zeihan - Millennial consumption.md)





# Thinking and Theories on Design

## Design Thinking Theories

## The Rhizome

## Deconstructionalism





- - -
## Working Thoughts

[Making Iron Tools without Iron Tools - Forging an Old Bloom](https://youtu.be/4VAry7BT6Sg)

* Should a designer be a profit or commodity maker; or should they be a philosopher and process maker?
* How can we tell what our purpose is, Capital Value confuses our reasonable logical ability to understand whether what we are doing is valuable on any metric other than financial.
* This has to change - Post-modernism appears to be an attempt to try.



### Internships - Placements

[Internships - Beme News](https://youtu.be/wz4Zsq1Z_Ls)


### How to Support yourself

[The daily Grind](https://youtu.be/aEhqAY_aNZc)







## What will being a thinker be defined or thought about in a century?

[Debussy Arabesques by Stefan Lindon](https://www.youtube.com/watch?v=eaDhVJnL7Fo&feature=youtu.be&fbclid=IwAR2Kl-KZRTB7zQ-gO3hAtt9VEjSnzMqseUbHPeE_hH1MJKbgKQrZanO2Kgk) - I commented on 25-04-2019 21:46 : "What are your opinions on Roger Hallam? How do you feel as a guinea pig in an experiment as data for a discourse on method?﻿"

Stefan Lindon manages the Extinction Rebellion Brighton Chapter so I thought I'd ask him what he thought of the man I think has created this thing. I have decided to do it on his youtube as it is a few steps away from his page in an attempt to point out the reach of the internet (and hopefully not to have proved his point that people would find his youtube if he did this (I would like to think that is not the case) and that if that was the case I should've gone deeper into his account, but I felt that an off Facebook action about a Facebook connected thing ought to have sent the message) - were this to have been the case : [This is where I'd have put it](https://www.youtube.com/watch?v=QPVkHir-gQk)



- - -

# Working Thoughts on The Etymology of Ideology

-ism

-icism

-ist

-istic

-ite

-ise

-ic : Used to form adjectives from nouns with the meaning “of or pertaining to”.

-is

-icy

[.](https://youtu.be/3WrJHdUewfU)


Communism, Communist, Communite, Communise, Communic, Communicise
- - -
Capitalism, Capitalist, Capitalite, Capitalise, Capitalic, Capitalicise



Capit - al - ism

* Capit (  /ˈka.put/, [ˈka.pʊt]  )  : 

1. head
2. (New Latin, anatomy) headlike protuberance on an organ or body part, usually bone, for instance caput ulnae
3. (New Latin, medicine) a disease; a severe swelling of the soft tissues of a newborn's scalp that develops as the baby travels through the birth canal
4. (figuratively) the vital part
5. (of a river) origin, source, head 
6. (figuratively) life
7. capital city 
8. (poetic) leader, chief
9. (in writings) division, section, paragraph, chapter

* -al (  /əl/  )  :

1. Of or pertaining to.
2. Forming nouns


* -ism (   /ɪzəm/, /ɪzm̩/  )  :

1. Used to form nouns of action or process or result based on the accompanying verb in -ise or -ize
2. Used to form the name of a system, school of thought or theory based on the name of its subkect or object or alternatively on the nameof its founder ((when de-capitalised, these overlap with the generic "doctrines" sense below))
3. Used to form names of a tendency of behaviour, action, state, condition or opinion belonging to a class or group of persons, or the result of a doctrine, ideology or principle of lack thereof.
4. Used to form nouns indicating a peculiarity or characteristic of language
5. used to form names of ideologies expressing belief in the superioriity within the concept expressed by the root word, or a pattern of behavior or a social norm that benefits members of the group indicated by the root word. ((based on a late 20th century narrowing of the "terms for a doctrine" sense))
6. (medicine) Used to form names of conditions or syndromes


Is this a linguistic neural network?





# The Communist Manifesto

A spectre is haunting Europe- the spectre of Communism. All the
powers of old Europe have entered into a holy alliance to exorcise this
spectre: Pope and Czar, Metternich and Guizot, French Radicals and German police spies.

Where is the party in opposition that has not been decried as
communistic by its opponents in power? Where the opposition that has not hurled back the branding reproach of Communism against the more advanced opposition parties, as well as against its reactionary adversaries?

Two things result from this fact:

I. Communism is already acknowledged by all European powers to be itself a power.

II. It is high time that Communists should openly, in the face of the whole world, publish their views, their aims, their tendencies, and meet this nursery tale of the spectre of Communism with a manifesto of the party itself.

To this end, Communists of various nationalities have assembled in London and sketched the following manifesto, to be published in the English, French, German, Italian, Flemish, and Danish languages.

## Why?

>To my academic colleagues: I implore you to look beyond your myopic careerist pursuits and throw your hats into the greater battle for the soul of our culture, our freedoms, our edifices of reason. Your silence makes you complicit in the degradation of our individual liberties. - [Gad Saad](https://twitter.com/GadSaad/status/1124504589158449153)


> [Bourgeois and Proletariat](Bourgois and Proletariat.md)

# The 'Capitalist' Manifesto

A spectre is haunting Europe - the spectre of Communism | Fascism. All the powers of the Old World have entered into a holy alliance to exorcise this spectre: Caliph and Czar; Western Centralisation and Eastern Junkerian Decentralisation; Eco-warriors and the modern battlefield of of the Internet flush with spies of the thought-police.  
Where is the party in opposition that has not been decried as communistic | fascistic by its opponents in power? Where is the opposition that has not hurled back the branding reproach of "Communist!" | "Fascist!" against the more advanced opposition parties, as well as against its reactionary adversaries?
The fact of the existence of the Communist manifesto as written by Marx and Engles cannot be disputed, and they brought forth two points from their analysis of Europe - I have added a couple of words (I think it'll be clear);

> Two things result from this fact:
>
>I. Communism | Fascism is already acknowledged by all European powers to be itself a power.
>
>II. It is high time that Communists | Fascists should be openly, in the face of the whole world, publish their views, their aims, their tenancies, and meet this nursery tale of the spectre of Communism | Fascism with a manifesto of the party itself.

Further from this a couple more points arise immediately in my mind, I am sure the length of the list of potential points is infinite, however:

A. The acknowledgement of the viability for the existence of alternate opinions is an imperative before a debate can exist.

B. The nature of the publication of manifesto for the expression of political belief should be retaken from the control systems by way of organised thought.

To this end, following the lead of Marx and Engles, and the example set by Woolf - "For masterpieces are not single and solitary births; they are the outcome of many years of thinking in common, of thinking by the body of the people, so that the experience of the mass is behind the single voice."

I, a Libertarian Capitalist | Democratic Confederalist, intend to sketch the following manifesto, along the principles set out by Engles and Marx in their manifesto, for the publication in English. 

This should serve to be essential reading for the Capital-Communist, or the Commune-Capitalist. Take it as it is, not for what it is.


# Words, and their definitions

## Capital and Capacities


Capital - Capital-ism, -ist, -etc...


Capacity - Capital 
(/kəˈpæsɪti/) - (/ˈkæp.ɪ.təl/)


### Capital

Compare Latinate capital and chattel, which also use “cow” to mean “property”, Literally, friends (“kith”) and cattle (“kine”). 


Alternative forms
capitall (obsolete)
Etymology
From Middle English capital, borrowed from Latin capitālis (“of the head”) (in sense “head of cattle”), from caput (“head”) (English cap). Use in trade and finance originated in Medieval economies when a common but expensive transaction involved trading heads of cattle.

Compare chattel and kith and kine (“all one’s possessions”), which also use “cow” to mean “property”.



### Capacity

From Middle English capacite, from Old French capacite, from Latin capācitās, from capax (“able to hold much”), from capiō (“to hold, to contain, to take, to understand”).




capacity (countable and uncountable, plural capacities)

The ability to hold, receive or absorb
A measure of such ability; volume
The maximum amount that can be held
It was hauling a capacity load.
The orchestra played to a capacity crowd.
Capability; the ability to perform some task
The maximum that can be produced.
Mental ability; the power to learn
A faculty; the potential for growth and development
A role; the position in which one functions
Legal authority (to make an arrest for example)
Electrical capacitance.
(operations) The maximum that can be produced on a machine or in a facility or group.
Its capacity rating was 150 tons per hour, but its actual maximum capacity was 200 tons per hour.


# Populism



## An investigation into what it is and what it is not

> Populism against Populism
	Authoritarian Populism vs Progressive Populism
	[Marianne 2020 Official Announcement](https://www.youtube.com/watch?v=ZsjqaSdRVuE)


[BYU Corpus search](https://www.english-corpora.org/now/)

### Populism search 1

* [For Lasch, the appeal of communitarianism and populism](https://www.theamericanconservative.com/articles/tac-bookshelf-for-the-week-of-july-30/)

#### TAC Bookshelf for the Week of July 30 (2018)

> Take, for example, Lasch’s chapter on communitarianism and populism. It’s striking that Lasch writes so forebodingly of liberalism’s internal contradictions amidst the backdrop of a liberal mid-90s economic scene of relative peace and prosperity. Yet for Lasch, the contradictions are too obvious to ignore. Both liberalism’s commitment to “progress” and its belief that a liberal state could dispense with civic virtue have as their premise that “capitalism had made it reasonable for everyone to aspire to a level of comfort formerly accessible only to the rich.” Neither the contemporary Right nor Left is immune from liberalism’s materialist bent; each side’s reliance on the remarkable growth of global GDP (the famed “hockey stick” graph) as an indicator of human flourishing reveals Lasch’s prescience.

> So what’s the alternative? For Lasch, the appeal of communitarianism and populism is apparent from liberalism’s contradictions. But this doesn’t mean that either of these two similar-yet-distinguishable traditions is the answer. Lasch displays a refreshing awareness for the role circumstance plays in politics, eschewing not principle, but policy masquerading as principle. A society that is thoroughly stratified between elites and the masses requires a different guiding philosophy than one instead threatened by communitarian overreach.

- - -

* [The project has identified that populism is the midwife of fascism](https://www.opendemocracy.net/en/opendemocracyuk/voters-deserve-vision-europe-not-cabinet-curiosities/)

#### Voters deserve a vision for Europe, not a cabinet of curiosities

> media-fed populism.
> The project has identified that populism is the midwife of fascism – and it is populism that is characterising these elections to a degree unprecedented in British elections.
> Populism is not primarily about being popular. Rather, it is about focusing on emotional responses rather than rational engagement.
> They [CUKs] are appealing to people’s disillusionment with our broken politics – and few can argue with that – but without being clear about what solutions they are offering. It is also concerning that two of their candidates had resigned within 48 hours of their launch, and a third is battling to stay on. This is another clear sign of populism, with poorly organised and weakly disciplined parties with no clear governance framework or due diligence.
> 
It appears to be a requirement for existence to erect a boundary within which existence continues from.

A problem, however, would appear to arise, upon the rejection of the requirement for there to be an area external from the boundary; which implicitly does not contain the interior. 

The functional problem of existing as if there are no externalities to the function of existence, would directly imply that there is no space outside of which existence does not exist.

Or as the religions have it; firmament, God. There would appear to be fuctionaly an external 'reality' mirroring the observable reality, as viewed by existence.

Existing requires an observable reality, one of which we project outwardly and inwardly, calling it sentience and putting ourselves atop a pile of observers.

The Godhead 'sees' through reality, over and around the inward and outward movements of reality. The upwardly and downwardly moving objects unobservable to the rotationally directed pattern finder as manifesting within a human condition.

The Capitalist society exists without an externality to its boundaries. The observation of the production of waste, is irrelevant, as waste is simply potential future resource. The system fails to notice however that it is possible to produce more waste than is functionally acceptable for the continuation of the system prior to the depletion of exhaustable resources.

The accessability of resources is diminished when in the form of new waste, as the resources are discretely ignorable as they do not represent quantifiable objects, as they are complex assemblages of many minor objects.

Resources are finite objects, as containing within them themselves, and the set of them - selves.

Waste is an *in*finite resource, as the compression into finitely analysable elements is less functionally important to the system than the continued expansion into unexplored markets.


## The Firmament and the Godhead

The firmament was the name religious texts gave to the boundary between; first, the heaven and the earth, then the solar system and the universe, before being abandoned upon the discovery of the wider universe.

The Godhead, and the functional image of God seeps through the firmament in streaking lines of brilliant light.

The Father, The Son, and The Holy Spirit, are the three sides around which a tetrahedron can be described; the boundary between the tetrahedron and the not-tetrahedron. Or mathematically the Vertices, The Edges, and The Interior.

We abandon the boundary between within and without with peril, the abandonment of an idea; as opposed to the holding an idea in opposition; suggests a fundamental misunderstanding as to the nature of arguments and rational discourse. 

Simply because an argument does not fit within ones current set of acceptable realities, does not make it abandonable, since, as the idea exists, it must be acceptable from some position. To abandon an idea internally, one abdicates their responsibility to tangle themselves inside it, to put the unacceptable idea functionally outside the arena of reality impacting ideas.

By abandoning the idea of the firmament however, or the boundary between [no|thing]; one generates a space where ideas can be functionally destroyed. Upon the emergence of functionally destroyed ideas [or observed planes of reality] in the observed world, creates a fight or flight dichotomous reaction. The ability to rationally tangle themselves within the idea is inaccessible as the idea has been placed without the functionally useful reality.

The idea themselves still exist, even when not considered. The question "If a tree falls in the woods, and no one was around to hear it. Did it make a sound?", still inspires thought, even though the question considers a functional idea outside the plane of reality. Considering that the functionally observable world is only catagorisable by what it is not; as the things themselves require more description than existence has time for given the detail observable.

Categorically moving all arguments rejected as personally unobservable; and therefore non-existent, transition to a plane best described by the word "firmament" - that place whereby reality ceases to exist; the place where unagreeable ideas are placed, so as to avoid the access to illogical connections.


### Godhead

The Godhead is the you that speaks, inside and outside. It is also the you that organises, describes order from chaos. It is contained within the bounds of an acceptable reality.

This acceptable reality however requires for its existence a duality - the place whereby all ideas are unacceptable.

The duality of heaven and hell in other words.

The hell of an idea, is the space where ideas are rejected on the basis of their being ideas. the heaven, the opposite. 

All actions however are not acceptable, and hence a plane of reality is required to manifest actions as connected extensions of ideas, such that the manifesting of categorisable actions can be attributable to gods given their functional interconnected nature to the plane of ideas [the internal reality].

Planes of existence are different, but connected. descrete, but tangental.

Ideas connect to their sets of corresponding extensions via action. Ideologies as organisms of Ideas, utilising the organs of groups within society. Ideas require actions, and actions require actors. Rational actors are not good enough, as the rational conclusion is simply the extension of the timespan for the effective act-ablity of the agent rather than the actions required by the system of the agent.

The tangental non-rational agent, can manifest goals as intermediary points towards a goal. A line is the connected extension of two points, a start and a goal, are two points. and as such the journey between those two points is an infinite interconnected system of infinite points. Such, the indirected tangental agent needs not move directly from start to goal, but instead in any direction. Assess the distance matrix from its initial position, and it's predicted relatable distance from the goal. 

The length of this process is arbitrary, the system will always, regardless reach its goal. The directed nature of a rational agent, requires the agent to disregard its goal for the maintenance of the direction of travel towards the goal.

Therefore irrational agents are better at reaching functional goals than rational agents.
# We Need to Talk About Universities.

#### They have become machines for profit under the guise of education.

	> Sep 27th 2018 at 12:24 pm

	Something has happened in my lifetime, but it certainly started a long time ago. Modernism, the Industrial Wars, and the advent of deconstructionism and neoliberalism has led to the creeping of wealth generation and profiteering on a global scale.

The capacity of human learning gives us a unique platform from which to exponentially increase the collective knowledge of the species. The advent of the internet seems to have taken us completely off guard, we are frantically trying to decide what to do with it. This new technology has reared its head and expanded itself into seemingly every part of the daily existence. 

This new capacity for the rapid accumulation of wealth through technologically assisted means, has led to the proliferation of market forces into all of our constructed systems. The barrier to a sustained existence is now a simple matter of pecuniary profitability. The argument is that those with value, will succeed within the global market, which itself is just based on our perception of value.

With that in mind, if the government exists and funds education to some level, should this education available be to the level which allows sustained existence within the state? From my place as a third year product design student, my previous educational experience leads me to believe that that is not the case. The state funds education with a goal oriented prospective towards a bachelor level qualification.

The problem, however, is that the university is now a place of bottom lines, margins, and results. The profit motive underlying administrative decision making will not lead to the flourishing of an innovative populous with a desire for further learning. But rather to the narrowing of learning, and a reduction in the developmental capacity of our collective knowledge. 

We are presented these places with broadening environments; containing new, creative, and novel methods of thinking and knowledge. How could we, and should we even; quantifiably compare disparate institutions, so as to ascertain their potential assets or potential longevity?

Instead of answering this question we seem to be blindly pressing on with our existing academic model. Students face the raising of fees, and the institutions need outward displays of their success rates to attract these new students. The State faces an influx of degree level educated people without an interest in knowledge I see from my parents and grandparents. This stumbling system, struggling under its own weight requires questioning.

We are effectively living in the “future”. The internet has led to a massive explosion in the ability of the human species to create, explore, and discover. On the fringes of advancement anything is becoming possible. We have to get over our obsession with the accumulation of wealth and control, it only seems to lead to the stifling of the potential of our collective innovation and collaborative potential.

The assimilation of neoliberal ideas into our collective cultural identity has contributed to the use of university administration as a financial engine rather than a machine tailored for intelligent education. Combined with this, we appear to be living in a time where the government can outsource the funding and management of education regulation to alleviate them from political interest and scrutiny. The progression of the price tagging of individuals has led to the development of a marketplace proliferated by our higher educational system, designed to mill through students to increase their incoming investment.

Our country urgently needs to discuss what the universities might be able to do, and who they might be for. We were sold Brexit based on its potential long term benefits, but despite the lack of real constructive discussion of the potential outcomes. Could it possibly be time to seriously take into account the long term sustainability of our society? 

And with that as our guide, we need to talk about the Universities, not for us, but for our collective future existence and development. 

We are surely doomed if the purpose of the education of our people is to create wealth rather than to educate us all for the benefit of ourselves as a whole.

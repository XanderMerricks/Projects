# Ai Image Generation and Upscaling


[AI Gahaku](https://ai-art.tokyo/en/)
 - This Ai produces 'portrait paintings' from images
*examples to come*

-------

[ImageUpscaler](https://imageupscaler.com/upscale-image-4x)
 - This algorithm upscales images
*examples to come*

-------

[icons8 image upscaler](https://icons8.com/upscaler)
 - This algorithm upscales images
*examples to come*

--------

[A Fully Progressive Approach to Single-Image Super-Resolution](https://igl.ethz.ch/projects/prosr/prosr-cvprw-2018-wang-et-al.pdf)
 - Research paper on an upsampling model
 - [Home page for above](https://igl.ethz.ch/projects/prosr/)
 - [Supplemental Material for above](https://igl.ethz.ch/projects/prosr/prosr-supplemental.pdf)
 - [git repository for above](https://github.com/fperazzi/proSR)

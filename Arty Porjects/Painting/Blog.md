# Painting

---

## Constancy of memory

	>Sep 25, 2018 at 8:44pm

![_MG_4784](_MG_4784.jpg)

I am currently painting. This is what it looked like last week before I went away for the weekend, the bright red acrylics glisten in the sunlight still blooming with moisture and possibility. 

...[read more](Constancy of memory.md)


## The Macro World

	> Sep 26, 2018 at 3:06pm

![_MG_4956](_MG_4956.jpg)

The parts of reality we think we can see are built from complex assemblages of things we cannot see. The optical lens allows our eyes to see further than by ourselves.
Here I have used an Olympus 28mm manual lens held in reverse with my Canon 400D...[read more](The Macro World.md)
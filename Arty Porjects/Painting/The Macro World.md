# The Macro World

	> Sep 26, 2018 at 3:06pm

![_MG_4956](_MG_4956.jpg)

The parts of reality we think we can see are built from complex assemblages of things we cannot see. The optical lens allows our eyes to see further than by ourselves.
Here I have used an Olympus 28mm manual lens held in reverse with my Canon 400D. I am going to be making a mount for this purpose to increase stability and my ability to focus on the photography than the clarity. 

On the other hand the border between clarity and diffusion leads our focus around images, the depths of distortion allows closer analysis of the subject.



The collection of photons by a digital sensor allows an extremely wide range of depictions of the real world. The view in the moment, and the resulting images can never be identical. The photons are paused in time until rendered to the viewer.



We have learnt how to stop time, to look backwards with a clarity unlike any other, but still this view is distorted, through the lenses distortions and errors are created, but within this process pure creativity abounds. The random scattering and landing of photons on a plain enriched and enhanced by digital minds give us a view into and out of what we could never hope to see sat, or stood gazing out through the glass.
# Constancy of memory 

	>Sep 25, 2018 at 8:44pm

![_MG_4784](_MG_4784.jpg)

I am currently painting. This is what it looked like last week before I went away for the weekend, the bright red acrylics glisten in the sunlight still blooming with moisture and possibility. 

The blue and black underlay is the destruction of a failed attempt at an expression of entirely other kind over a year ago.  The under image still protrudes through, but surely only I can know when what came where. 

Or is that even possible? How can we have any certainty as to the constancy and congruence of our memories. Surely it's there somewhere, just not sorted in a pattern the conscious mind can recognise? 

I think I know what came where now because I know what wasn't there last month, but even now what I see beside me and what I've photographed I can see what went where when in some sense because I can see specifically what is where now and what was not where then. 

Photographic realism or acrylic and wax on canvas? 
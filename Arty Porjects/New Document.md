# Bookmarks | Links | Documents | etc.

## Website stuff

* [Blue Host](https://www.bluehost.com/)
* 



## Printing services

* [Redcliff Imaging](https://www.custom-printed-cards.co.uk/index.html)



## Photography / Video Editing Tools

* [Exif Tool Gui](https://exiftool.org/gui/)
* [Hybrid Images](https://franciscouzo.github.io/hybrid/)
* [Shutterstock image resizer](https://www.shutterstock.com/image-resizer)


## Photography / Video / Editing Tutorials / Guides

* [Partial Photsphere](https://360rumors.com/technique-how-to-make-partia/)
* [Setting the tilt axis for Tilt-Shift lenses](http://www.northlight-images.co.uk/setting-the-tilt-axis-for-tilt-shift-lenses/)



## Colours

* [Colormind Image based Colour Palette Generator](http://colormind.io/image/)
* 


## PDFs - Academia - Miscellaneous

* [Deconstructing the High Line: The Politics of Nature (pdf)](\New_Links\Deconstructing_the_High_Line_the_Politic.pdf)
* [HSLa Grid](https://css-tricks.com/examples/HSLaExplorer/)



## Music etc.

* [Chet Akins "Jam Man"](https://www.youtube.com/watch?v=UuYwvolnBm0&ab_channel=daffydoug)
* [Full English - A Collection of Traditional British Folk Songs](https://www.youtube.com/watch?v=LyNjoNEWVNo&list=PLpvnuBpObavctTiMlDtpDDOQ6b6IAbFHy&index=3&ab_channel=MatWilliams-Topic)
* [Pink Siifu Interview](https://www.youtube.com/watch?v=ciYjLigncXI&ab_channel=WRSU-RutgersRadio)
* [Pink Siifu - Stay Sane](https://youtu.be/MzSu5zyZWHo)
* [Grook · Snazzback · China Bowls](https://www.youtube.com/watch?v=IOpiSMHJxyU&ab_channel=Snazzback-Topic)
* [Snazzback - Hedge](https://snazzback.bandcamp.com/album/hedge)

## Poetry

* [T.S. Eliot - The Waste Land](https://www.poetryfoundation.org/poems/47311/the-waste-land)
* [Dylan Thomas - Do not go gentle into that good night](https://poets.org/poem/do-not-go-gentle-good-night)

## Politics and Society

* [Campaign for an English Parliament - Aims, Principles and Policies](https://thecep.org.uk/aims-principles-and-policies/)
* [EDP - Manifesto](https://www.englishdemocrats.party/manifesto-posts) - [pdf](https://drive.google.com/viewerng/viewer?url=https://d3n8a8pro7vhmx.cloudfront.net/englishdemocrats/pages/30/attachments/original/1613243611/EDP_Manifesto_February_21.pdf?1613243611)
* 




## Design Project Inspirations | Concepts

* [Derek Hugger - Fescue](https://www.derekhugger.com/fescue.html)
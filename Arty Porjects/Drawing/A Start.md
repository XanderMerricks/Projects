# A Start

	> Sep 29, 2018 at 12:51pm

![_MG_5081](_MG_5081.jpg)

I bought a 5m roll of a meter wide packing paper to stick up on the wall to draw on. I can never keep my desk clear or tidy enough to have space for paper, pens, and pencils. 


I also have my projector back - I am planning on doing some drawings around projected images, and layered sections.



The head here is drawn from a projected photograph in two parts. 

![_MG_5073](_MG_5073.jpg)

 The stuck up sheets are where the layering is going to begin. I intend on adding new sheets for the new drawings building up layers of sectioned views of drawings as old parts are covered and new parts trickle through.



I am going to try to find a way to timelapse the creation of these images, but due to the nature of my projector it is far easier to work at night, when it is harder to photograph due to lack of light.



I also need to build some platforms so the projections are less wibbly than they are at the moment, it is also not possible to take consistent photographs of the scene without a tripod in the way or a platform for the camera.



I may add some colour in the future, but the charcoal and chalks are something different I've not really worked with before. 



We shall see how this goes.

![_MG_5158](_MG_5158.jpg)
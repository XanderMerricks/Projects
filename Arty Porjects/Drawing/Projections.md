
# Projections

	> Oct 1, 2018 at 2:07am

![_MG_5154](_MG_5154.jpg)

I've been using the projector like this, it's been an interesting thing.


I've made an animation using frames taken during this process, this is from somewhere in there.

![1.2](1.2.gif)

In due course I'll be creating a youtube channel or something similar to put these animations. I'll post later about that.



I am planning on doing something more intricate or longer in time using a video camera to log the progress so I don't have to decide in the moment where I capture what with the camera remote producing stills. My camera is set up for photography rather than frame capture for animation.

![_MG_3928](_MG_3928.jpg)

I also need to create something to hold the projector such that I can move it about, I may look at the possibility of suspending it in someway it's only little after all.
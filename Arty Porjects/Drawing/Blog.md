# Drawing

---

# Projections

	> Oct 1, 2018 at 2:07am

![_MG_5154](_MG_5154.jpg)

I've been using the projector like this, it's been an interesting thing...[read more](Projections.md)



# A Start

	> Sep 29, 2018 at 12:51pm

![_MG_5081](_MG_5081.jpg)

I bought a 5m roll of a meter wide packing paper to stick up on the wall to draw on. I can never keep my desk clear or tidy enough to have space for paper, pens, and pencils. 


I also have my projector back - I am planning on doing some drawings around projected images, and layered sections...[read more](A Start.md)
# Introduction

This is an introduction page.

- - -

[![William Kentridge Second Hand Reading](http://img.youtube.com/vi/l051iRzDqqE/0.jpg)](https://www.youtube.com/watch?v=l051iRzDqqE)

> This is William Kentridge's Second Hand Reading - I filmed it in March 2019 on the final day of the exhibition in Manchester at the Whitworth Gallery.
> I would consider it to be the starting point of the revolution in thought about myself as a designer and my place in the world of design.
> I went to see this exhibition twice once before the new year and once afterwards.
> There is something about the piece 'the refusal of time' which got stuck into my head, and has influenced my every thought since
- - -

There are many many things contained within this 'book' 

I hope you Enjoy.




[![Animation Test 1](http://img.youtube.com/vi/aLy7vkiD0NY/0.jpg)](https://www.youtube.com/watch?v=aLy7vkiD0NY)

The image above is a link to my first animation (unless I got my links backwards)